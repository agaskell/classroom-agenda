module ActionView
  module Helpers
    module FormHelper
      def text_area_with_defaults(object_name, method, options = {})
        default_options = {}
        default_options[:rows] = 6
        default_options[:cols] = 35
        default_options.merge!(options)
        text_area_without_defaults object_name, method, default_options
      end
      alias_method_chain :text_area, :defaults
    end
    
    module FormTagHelper
      def text_area_tag_with_defaults(name, content = nil, options = {})
        default_options = {}
        default_options[:rows] = 6
        default_options[:cols] = 35
        default_options.merge!(options)
        text_area_without_defaults name, method, default_options
      end    
      alias_method_chain :text_area_tag, :defaults
    end
  end
end

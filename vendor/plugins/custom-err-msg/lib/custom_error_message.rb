module ActiveRecord
  class Error
  #monkey patched to allow for messages of the form "^This is my message"
    def full_message
      attribute.to_s == 'base' ? message : (message =~ /^\^/ ? message[1..-1] : generate_full_message(message, options.dup))
    end
  end
end

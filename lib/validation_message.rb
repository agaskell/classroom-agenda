module ValidationMessage
  def validation_header
    "Please try again!"
  end 

  def validation_message
    action = self.new_record? ? "setting up" : "updating"
    "We had some problems #{action} your #{self.class.table_name.singularize.humanize.downcase}"
  end
end

module Assetable
  AttachmentLimit = 10
  
  def web_assets
    @web_assets ||= assets.select { |a| a.web_safe? }
  end
  
  def file_assets
    @file_assets ||= assets.select { |a| !a.web_safe? }
  end
  
  def update_attachings  
    teacher = self.respond_to?("teacher") ? self.teacher : self.classrooms[0].classroom_group.teacher
    self.assets.each do |asset|
      ta = Attaching.new { |a| a.attachable_type = teacher.class.name; a.attachable_id = teacher.id }
      aa = Attaching.new { |a| a.attachable_type =  self.class.name; a.attachable_id = self.id }

      asset.attachings.push(ta)  if asset.attachings.select { |a| a.attachable_type == ta.attachable_type && a.attachable_id == ta.attachable_id }.empty?   
      asset.attachings.push(aa) if asset.attachings.select { |a| a.attachable_type == aa.attachable_type && a.attachable_id == aa.attachable_id }.empty?
    end
  end   
  
  def has_new_attachment?
    !data.nil?
  end
  
  def attachment_limit_reached?
    t = classrooms[0].classroom_group.teacher
    return false if t.has_premium_plan?
    t.assets.count >= AttachmentLimit
  end
  
  def validate
    if has_new_attachment? && attachment_limit_reached?
      errors.add :attachment, "^The free plan offers 10 file attachments. You've reached this limit - If you'd like to upload new attachments you can delete some existing attachments or upgrade your plan."
    end
  end
  
  def after_save
    super
    update_attachings
  end
  
end

namespace :db do
  desc 'Clears out demo accounts older than a day and cleans the database'
  task :demo => :environment do
  
  i = User.find(:all).count
  User.find(:all, :conditions => ["is_demo = 1 AND created_at < ?", DateTime.now - 1.day]).each { |u| u.destroy }
  puts "Removed #{i - User.find(:all).count} users."

  Assignment.find(:all).each { |a| puts a.destroy if a.classroom_ids.empty? }
  FieldTrip.find(:all).each { |a| puts a.destroy if a.classroom_ids.empty? }
  Exam.find(:all).each { |a| puts a.destroy if a.classroom_ids.empty? }
  
=begin
    ActiveRecord::Base.record_timestamps = false
  
    puts 'Loading data...'
    user = User.find_by_login("demo")
    user.destroy unless user.nil?
    
    user = User.new do |u| 
      u.login = "demo"
      u.email = "demo@classroomagenda.com"
      u.first_name = "Demo"
      u.last_name = "Teacher"
      u.password = "demo"
      u.password_confirmation = "demo"
    end
    
    user.build_teacher(:salutation => "Mrs Carbone", :bio => "I love IHOP!")
    user.teacher.download_remote_image("http://photos-e.ak.fbcdn.net/hphotos-ak-snc1/hs013.snc1/4482_179799855050_545590050_7017420_1976868_n.jpg")
    
    user.teacher.plan = SubscriptionPlan.find_by_name("Premium")

    user.teacher.announcements.build(:title => "Welcome to Classroom Agenda!", 
      :details => "I hope you find Classroom Agenda useful. I think you will. Enjoy your stay!", 
      :start_date => Date.today, 
      :end_date => Date.today + 2.days, 
      :updated_at => DateTime.now, 
      :created_at => DateTime.now)

    user.teacher.announcements.build(:title => "Hi.", 
      :details => "My name is Andy and I built Classroom Agenda. Right now it's 11:55 PM on August 8th, 2009. I'm working on writing what you are reading this very moment - so far it's going pretty well, but coming up with fake classes, assignments, exams, and field trips is tough!", 
      :start_date => Date.today - 1.month, 
      :end_date => Date.today - 34.days, 
      :updated_at => DateTime.now, 
      :created_at => DateTime.now)
      
    user.teacher.save!
    user.save!
    
    user.teacher.classroom_groups.create(:name => "Fall 2009",
      :is_active => true,
      :display => true,
      :position => 1)
    
    user.teacher.classroom_groups[0].classrooms.create(:name => "Math",
        :display => true,
        :position => 1)

    Assignment.create(:title => "Worksheet 1 - Odd only",
          :details => "Read pages 11 - 14 in your book. Do all odd problems on worksheet #1. Show your work!",
          :start_date => Date.yesterday,
          :due_date => Date.tomorrow,
          :classroom_ids => [user.teacher.classroom_groups[0].classrooms[0].id])

    Assignment.create(:title => "Worksheet 1 - Even only",
          :details => "Read pages 11 - 14 in your book. Do all even problems on worksheet #1. Show your work!",
          :start_date => Date.today - 2.days,
          :due_date => Date.today, 
          :classroom_ids => [user.teacher.classroom_groups[0].classrooms[0].id] )

    Assignment.create(:title => "Worksheet 2 - All problems",
          :details => "Read pages 17 - 24 in your book. Do all even problems on worksheet #2. Show your work!",
          :start_date => Date.tomorrow,
          :due_date => Date.today + 2.days, 
          :classroom_ids => [user.teacher.classroom_groups[0].classrooms[0].id] )

    Exam.create(:title => "Chapter 1 Exam",
          :details => "Test will cover all of chapter 1. Test is not open book but calculators are allowed.",
          :exam_date => Date.today + 1.week,
          :classroom_ids => [user.teacher.classroom_groups[0].classrooms[0].id] )

    user.teacher.classroom_groups[0].classrooms.create(:name => "Reading",
          :display => true,
          :position => 2)

    Assignment.create(:title => "Chapter 1",
          :details => "Read chapter 1 of Huckleberry Finn. Be prepared to discuss in class.",
          :start_date => Date.today - 3.days,
          :due_date => Date.today,
          :classroom_ids => [user.teacher.classroom_groups[0].classrooms[1].id] )

    Assignment.create(:title => "Chapter 2",
          :details => "Read chapter 2 of Huckleberry Finn. Be prepared to discuss in class.",
          :start_date => Date.today - 3.days,
          :due_date => Date.tomorrow,
          :classroom_ids => [user.teacher.classroom_groups[0].classrooms[1].id] )

    user.teacher.classroom_groups[0].classrooms.create(:name => "Science",
          :display => true,
          :position => 3)

    Assignment.create(:title => "Read Chapter 3 and Quiz",
          :details => "Read chapter 3 and take the end of chapter quiz.",
          :start_date => Date.yesterday,
          :due_date => Date.today + 4.days,
          :classroom_ids => [user.teacher.classroom_groups[0].classrooms[2].id] )

    Exam.create(:title => "Chapter 3 Exam",
          :details => "Test will cover all of chapter 3 and will be open book.",
          :exam_date => Date.today,
          :classroom_ids => [user.teacher.classroom_groups[0].classrooms[2].id] )

    user.teacher.classroom_groups[0].classrooms.create(:name => "Social Studies",
          :display => true,
          :position => 4)

    Assignment.create(:title => "State Capital Worksheet",
          :details => "Complete the state capital worksheet.",
          :start_date => Date.today - 3.days,
          :due_date => Date.today,
          :classroom_ids => [user.teacher.classroom_groups[0].classrooms[3].id] )

    Exam.create(:title => "State Capitals A-G",
          :details => "You will need to locate states that start with A-G and know their capitals.",
          :exam_date => Date.tomorrow,
          :classroom_ids => [user.teacher.classroom_groups[0].classrooms[3].id] )
          
    FieldTrip.create(:title => "Science Museum", 
          :details => "The bus leaves at 9:30 and returns at 2:30. Remember to bring a bag lunch.",
          :trip_date => Date.today + 1.week,
          :classroom_ids => user.teacher.classroom_groups[0].classroom_ids )

    FieldTrip.create(:title => "State Capital",
          :details => "The bus leaves at 9:30 and returns at 2:30. Remember to bring a bag lunch.",
          :trip_date => Date.today + 18.days,
          :classroom_ids => user.teacher.classroom_groups[0].classroom_ids )
=end          
  end
end

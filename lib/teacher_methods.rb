module TeacherMethods

  def unread_message_count
    messages.count(:conditions => "`messages`.read IS NULL OR `messages`.read = 0")
  end

  def calendar_settings
    @calendar_settings ||= CalendarSettings.new
  end  
  
  def calendar_settings=(c)
    @calendar_settings = c
  end

  def active_classroom_group
    @group ||= classroom_groups.find_by_is_active(true) #(:all, :conditions => {:is_active => true})
  end
  
  def active_classrooms
    active_classroom_group.nil? ? [] : active_classroom_group.classrooms
  end
  
  def is_teachers_assignment?(assignment)
    (classrooms & assignment.classrooms).length
  end
  
  def all_classroom_ids 
    @all_classroom_ids ||= classrooms.collect {|c| c.id}
  end

  def active_classroom_ids
    @active_classroom_ids ||= active_classrooms.collect{|c| c.id}
  end
  
  def search_classroom_ids
    if @search_classroom_ids.nil?
      if calendar_settings.c.empty?
        @search_classroom_ids = active_classroom_ids 
      else
        @search_classroom_ids = active_classroom_ids & calendar_settings.c.select { |k, v| v == 1 }.map { |a| a[0].to_i }
      end
    end
    @search_classroom_ids
  end
  
  def has_premium_plan?
    @is_premium ||= subscription.subscription_plan.amount > 0
  end
  
  #Announcements
  def all_announcements
    @announcements ||= calendar_settings.show_announcements? ? announcements : []
  end  
  
  def announcements_by_date(date = Date.current)
    calendar_settings.show_announcements? ? announcements.select { |a| !a.start_date.nil? && !a.end_date.nil? && a.start_date <= date && a.end_date >= date } : []
  end
  
  #Exams
  def all_exams
    @exams ||= calendar_settings.show_exams? ? find_by_class(Exam, search_classroom_ids) : []
  end
  
  def all_active_exams
    @exams ||= find_by_class(Exam)
  end
  
  def todays_exams
    @todays_exams ||= exams_by_date
  end
  
  def tomorrows_exams
    @tomorrows_exams ||= exams_by_date(Date.tomorrow)
  end  

  def upcoming_exams
    find_by_class_and_not_expired(Exam, 'exam_date')    
  end
  
  def exams_by_updated_at(date = Date.current)
    calendar_settings.show_announcements? ? find_by_class_and_updated_at(Exam, date) : []
  end

  def exams_by_created_at(date = Date.current)
    calendar_settings.show_announcements? ? find_by_class_and_created_at(Exam, date) : []
  end

  def exams_by_date(date = Date.current)
    calendar_settings.show_exams? ? find_by_class_and_target_date(Exam, 'exam_date', date) : []
  end
  
  #Assignments  
  def all_assignments
    @assignments ||= calendar_settings.show_assignments? ? find_by_class(Assignment, search_classroom_ids) : []
  end

  def all_active_assignments
    @assignments ||= find_by_class(Assignment)
  end

  def open_assignments
    find_by_class_and_not_expired(Assignment, 'due_date')
  end

  def todays_assignments
    @todays_assignments ||= assignments_by_date
  end
  
  def tomorrows_assignments
    @tomorrows_assignments ||= assignments_by_date(Date.tomorrow)
  end
  
  def assignments_by_updated_at(date = Date.current)
    calendar_settings.show_announcements? ? find_by_class_and_updated_at(Assignment, date) : []
  end

  def assignments_by_created_at(date = Date.current)
    calendar_settings.show_announcements? ? find_by_class_and_created_at(Assignment, date) : []
  end

  def assignments_by_date(date = Date.current)
    calendar_settings.show_assignments? ? find_by_class_and_target_date(Assignment, 'due_date', date) : []
  end

  #Field Trips
  def all_field_trips
    @field_trips ||= calendar_settings.show_field_trips? ? find_by_class(FieldTrip, search_classroom_ids) : []
  end  

  def all_active_field_trips
    @field_trips ||= find_by_class(FieldTrip)
  end  
  
  def todays_field_trips
    @todays_field_trips ||= field_trips_by_date
  end

  def tomorrows_field_trips
    @tomorrows_field_trips ||= field_trips_by_date(Date.tomorrow)
  end
  
  def upcoming_field_trips
    find_by_class_and_not_expired(FieldTrip, 'trip_date')
  end
  
  def field_trips_by_date(date = Date.current)
    calendar_settings.show_field_trips? ? find_by_class_and_target_date(FieldTrip, 'trip_date', date) : []
  end

  def field_trips_by_updated_at(date = Date.current)
    calendar_settings.show_announcements? ? find_by_class_and_updated_at(FieldTrip, date) : []
  end

  def field_trips_by_created_at(date = Date.current)
    calendar_settings.show_announcements? ? find_by_class_and_created_at(FieldTrip, date) : []
  end  
  
  def date_feed(date = Date.current, calendar_settings = CalendarSettings.new)
    @date_feed = (exams_by_created_at(date) + exams_by_updated_at(date) + 
                     field_trips_by_created_at(date) + field_trips_by_updated_at(date) + 
                     assignments_by_created_at(date) + assignments_by_updated_at(date) +
                     announcements_by_date(date)).sort_by { |a| - a.updated_at.to_i }
  end
  
  def all_events
    @all_events ||= all_announcements + all_assignments + all_exams + all_field_trips
  end

  def event_on?(date = Date.current)
    @all_ary ||= all_events.to_ary
    o = @all_ary.find do |e| 
      if e.respond_to?('target_date')
        !e.target_date.nil? && e.target_date == date   
      else
        if e.respond_to?('start_date') && e.respond_to?('end_date')
          !e.start_date.nil? && !e.end_date.nil? && e.start_date <= date && date <= e.end_date
        else
          if e.check_timestamps && e.respond_to?('updated_at') && !e.updated_at.nil?
            e.updated_at >= date && e.updated_at < date.tomorrow
          else
            false
          end
        end
      end
    end
     # }
    #events = all_events.select { |e| e.target_date == date } 
    return !o.nil? #&& events.length > 0
  end

  #Generic Helpers   
  def find_by_class(klass, classroom_ids = active_classroom_ids)
    klass.find(:all, :select => "distinct #{klass.table_name}.*", :joins => [:classrooms], :conditions => ["(classrooms.id in (?))", classroom_ids])        
  end
  
  def find_by_class_and_not_expired(klass, date_field, date = Date.current)
    klass.find(:all, :select => "distinct #{klass.table_name}.*", :joins => [:classrooms], :conditions => ["(#{klass.table_name}.#{date_field} >= ?) & (classrooms.id in (?))", date, search_classroom_ids])    
  end
  
  def find_by_class_and_target_date(klass, date_field, date = Date.current)
    klass.find(:all, :select => "distinct #{klass.table_name}.*", :joins => [:classrooms], :conditions => ["(#{klass.table_name}.#{date_field} >= ?) & (#{klass.table_name}.#{date_field} < ?) & (classrooms.id in (?))", date, date.tomorrow, search_classroom_ids])
  end

  def find_by_class_and_updated_at(klass, date = Date.current)
    klass.find(:all, :select => "distinct #{klass.table_name}.*", :joins => [:classrooms], :conditions => ["(#{klass.table_name}.updated_at >= ?) & (#{klass.table_name}.updated_at < ?) & (#{klass.table_name}.updated_at != #{klass.table_name}.created_at) & (classrooms.id in (?))", date, date.tomorrow, search_classroom_ids])
  end

  def find_by_class_and_created_at(klass, date = Date.current)
    klass.find(:all, :select => "distinct #{klass.table_name}.*", :joins => [:classrooms], :conditions => ["(#{klass.table_name}.created_at >= ?) & (#{klass.table_name}.created_at < ?) & (#{klass.table_name}.updated_at = #{klass.table_name}.created_at) & (classrooms.id in (?))", date, date.tomorrow, search_classroom_ids])
    #find_by_class_and_target_date(klass, 'created_at', date)
  end
end

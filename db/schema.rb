# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20090822235942) do

  create_table "announcements", :force => true do |t|
    t.string   "title"
    t.text     "details"
    t.integer  "teacher_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "start_date"
    t.date     "end_date"
  end

  add_index "announcements", ["teacher_id"], :name => "index_announcements_on_teacher_id"

  create_table "assets", :force => true do |t|
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "attachings_count",  :default => 0
    t.datetime "created_at"
    t.datetime "data_updated_at"
    t.string   "link_name"
  end

  create_table "assignments", :force => true do |t|
    t.string   "title"
    t.text     "details"
    t.date     "start_date"
    t.date     "due_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "assignments_classrooms", :id => false, :force => true do |t|
    t.integer "assignment_id"
    t.integer "classroom_id"
  end

  add_index "assignments_classrooms", ["assignment_id"], :name => "index_assignments_classrooms_on_assignment_id"
  add_index "assignments_classrooms", ["classroom_id"], :name => "index_assignments_classrooms_on_classroom_id"

  create_table "attachings", :force => true do |t|
    t.integer  "attachable_id"
    t.integer  "asset_id"
    t.string   "attachable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "attachings", ["asset_id"], :name => "index_attachings_on_asset_id"
  add_index "attachings", ["attachable_id"], :name => "index_attachings_on_attachable_id"

  create_table "classroom_groups", :force => true do |t|
    t.string   "name"
    t.boolean  "is_active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "teacher_id"
    t.integer  "position"
    t.boolean  "display"
  end

  add_index "classroom_groups", ["teacher_id"], :name => "index_classroom_groups_on_teacher_id"

  create_table "classrooms", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "classroom_group_id"
    t.integer  "position"
    t.boolean  "display"
  end

  add_index "classrooms", ["classroom_group_id"], :name => "index_classrooms_on_classroom_group_id"

  create_table "classrooms_exams", :id => false, :force => true do |t|
    t.integer "classroom_id"
    t.integer "exam_id"
  end

  add_index "classrooms_exams", ["classroom_id"], :name => "index_classrooms_exams_on_classroom_id"
  add_index "classrooms_exams", ["exam_id"], :name => "index_classrooms_exams_on_exam_id"

  create_table "classrooms_field_trips", :id => false, :force => true do |t|
    t.integer "classroom_id"
    t.integer "field_trip_id"
  end

  add_index "classrooms_field_trips", ["classroom_id"], :name => "index_classrooms_field_trips_on_classroom_id"
  add_index "classrooms_field_trips", ["field_trip_id"], :name => "index_classrooms_field_trips_on_field_trip_id"

  create_table "exams", :force => true do |t|
    t.string   "title"
    t.text     "details"
    t.date     "exam_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "field_trips", :force => true do |t|
    t.string   "title"
    t.text     "details"
    t.date     "trip_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", :force => true do |t|
    t.integer  "teacher_id"
    t.string   "email"
    t.text     "body"
    t.string   "ip_address"
    t.boolean  "read"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "open_id_authentication_associations", :force => true do |t|
    t.integer "issued"
    t.integer "lifetime"
    t.string  "handle"
    t.string  "assoc_type"
    t.binary  "server_url"
    t.binary  "secret"
  end

  create_table "open_id_authentication_nonces", :force => true do |t|
    t.integer "timestamp",  :null => false
    t.string  "server_url"
    t.string  "salt",       :null => false
  end

  create_table "password_resets", :force => true do |t|
    t.string   "email"
    t.integer  "user_id"
    t.string   "remote_ip"
    t.string   "token"
    t.datetime "created_at"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "subscription_discounts", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.decimal  "amount",                 :precision => 6, :scale => 2, :default => 0.0
    t.boolean  "percent"
    t.date     "start_on"
    t.date     "end_on"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "apply_to_setup",                                       :default => true
    t.boolean  "apply_to_recurring",                                   :default => true
    t.integer  "trial_period_extension",                               :default => 0
  end

  create_table "subscription_payments", :force => true do |t|
    t.integer  "teacher_id"
    t.integer  "subscription_id"
    t.decimal  "amount",          :precision => 10, :scale => 2, :default => 0.0
    t.string   "transaction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "setup"
    t.boolean  "misc"
  end

  add_index "subscription_payments", ["subscription_id"], :name => "index_subscription_payments_on_subscription_id"
  add_index "subscription_payments", ["teacher_id"], :name => "index_subscription_payments_on_teacher_id"

  create_table "subscription_plans", :force => true do |t|
    t.string   "name"
    t.decimal  "amount",         :precision => 10, :scale => 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "renewal_period",                                :default => 1
    t.decimal  "setup_amount",   :precision => 10, :scale => 2
    t.integer  "trial_period",                                  :default => 1
  end

  create_table "subscriptions", :force => true do |t|
    t.decimal  "amount",                   :precision => 10, :scale => 2
    t.datetime "next_renewal_at"
    t.string   "card_number"
    t.string   "card_expiration"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state",                                                   :default => "trial"
    t.integer  "subscription_plan_id"
    t.integer  "teacher_id"
    t.integer  "renewal_period",                                          :default => 1
    t.string   "billing_id"
    t.integer  "subscription_discount_id"
  end

  add_index "subscriptions", ["teacher_id"], :name => "index_subscriptions_on_teacher_id"

  create_table "teachers", :force => true do |t|
    t.integer  "user_id"
    t.string   "salutation"
    t.text     "bio"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.boolean  "display_field_trips",   :default => true
    t.boolean  "display_assignments",   :default => true
    t.boolean  "display_exams",         :default => true
    t.boolean  "display_announcements", :default => true
    t.boolean  "display_calendar",      :default => true
  end

  add_index "teachers", ["user_id"], :name => "index_teachers_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "login"
    t.string   "email"
    t.string   "crypted_password",          :limit => 40
    t.string   "salt",                      :limit => 40
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token"
    t.datetime "remember_token_expires_at"
    t.integer  "user_type_id"
    t.string   "user_type_type"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "identifier"
    t.boolean  "is_demo",                                 :default => false
  end

end

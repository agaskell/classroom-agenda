class AddDisplayOrderColumn < ActiveRecord::Migration
  def self.up
    add_column :classrooms, :display_order, :integer
    add_column :classroom_groups, :display_order, :integer
  end

  def self.down
    remove_column :classrooms, :display_order
    remove_column :classroom_groups, :display_order
  end
end

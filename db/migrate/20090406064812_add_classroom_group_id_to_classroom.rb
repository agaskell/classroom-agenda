class AddClassroomGroupIdToClassroom < ActiveRecord::Migration
  def self.up
    add_column :classrooms, :classroom_group_id, :integer
    
  end

  def self.down
    remove_column :classrooms, :classroom_group_id
  end
end

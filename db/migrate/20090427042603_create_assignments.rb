class CreateAssignments < ActiveRecord::Migration
  def self.up
    create_table :assignments do |t|
      t.string :title
      t.text :details
      t.date :start_date
      t.date :due_date

      t.timestamps
    end
  end

  def self.down
    drop_table :assignments
  end
end

class AddIdentifierToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :identifier, :string
    remove_column :users, :identity_url
  end

  def self.down
    remove_column :users, :identifier
    add_column :users, :identity_url, :string
  end
end

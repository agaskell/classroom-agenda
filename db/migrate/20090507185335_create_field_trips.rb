class CreateFieldTrips < ActiveRecord::Migration
  def self.up
    create_table :field_trips do |t|
      t.string :title
      t.text :details
      t.date :trip_date

      t.timestamps
    end
  end

  def self.down
    drop_table :field_trips
  end
end

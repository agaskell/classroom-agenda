class CreateSubscriptions < ActiveRecord::Migration
  def self.up
    create_table "subscription_discounts", :force => true do |t|
      t.string   "name"
      t.string   "code"
      t.decimal  "amount",             :precision => 6, :scale => 2, :default => 0.0
      t.boolean  "percent"
      t.date     "start_on"
      t.date     "end_on"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "apply_to_setup",                                   :default => true
      t.boolean  "apply_to_recurring",                               :default => true
      t.integer  "trial_period_extension", :default => 0
    end

    create_table "subscription_payments", :force => true do |t|
      t.integer  "teacher_id",      :limit => 11
      t.integer  "subscription_id", :limit => 11
      t.decimal  "amount",                        :precision => 10, :scale => 2, :default => 0.0
      t.string   "transaction_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.boolean  "setup"
      t.boolean  "misc"
    end
    
    add_index 'subscription_payments', 'teacher_id'
    add_index 'subscription_payments', 'subscription_id'

    create_table "subscription_plans", :force => true do |t|
      t.string   "name"
      t.decimal  "amount",                       :precision => 10, :scale => 2
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "renewal_period", :limit => 11,                                :default => 1
      t.decimal  "setup_amount",                 :precision => 10, :scale => 2
      t.integer  "trial_period",   :limit => 11,                                :default => 1
    end

    create_table "subscriptions", :force => true do |t|
      t.decimal  "amount",                                 :precision => 10, :scale => 2
      t.datetime "next_renewal_at"
      t.string   "card_number"
      t.string   "card_expiration"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string   "state",                                                                 :default => "trial"
      t.integer  "subscription_plan_id",     :limit => 11
      t.integer  "teacher_id",               :limit => 11
      t.integer  "renewal_period",           :limit => 11,                                :default => 1
      t.string   "billing_id"
      t.integer  "subscription_discount_id", :limit => 11
    end
    
    add_index 'subscriptions', 'teacher_id'
  end

  def self.down
    remove_index 'subscriptions', 'teacher_id'
    drop_table :subscriptions
    drop_table :subscription_plans
    remove_index 'subscription_payments', 'subscription_id'
    remove_index 'subscription_payments', 'teacher_id'
    drop_table :subscription_payments
    drop_table :subscription_discounts
  end
end

class AddDisplayColumn < ActiveRecord::Migration
  def self.up
    add_column :classrooms, :display, :boolean
    add_column :classroom_groups, :display, :boolean
  end

  def self.down
    remove_column :classrooms, :display
    remove_column :classroom_groups, :display
  end
end

class RenameDisplayOrderColumns < ActiveRecord::Migration
  def self.up
    rename_column "classroom_groups", "display_order", "position"
    rename_column "classrooms", "display_order", "position"
  end

  def self.down
    rename_column "classroom_groups", "position", "display_order"
    rename_column "classrooms", "position", "display_order"
  end
end

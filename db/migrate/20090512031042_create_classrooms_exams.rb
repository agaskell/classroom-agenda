class CreateClassroomsExams < ActiveRecord::Migration
  def self.up
    create_table :classrooms_exams, :id => false do |t|
        t.integer :classroom_id
        t.integer :exam_id
    end    
  end

  def self.down
    drop_table :classrooms_exams    
  end
end

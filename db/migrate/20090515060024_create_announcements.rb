class CreateAnnouncements < ActiveRecord::Migration
  def self.up
    create_table :announcements do |t|
      t.string :title
      t.text :details
      t.date :target_date
      t.integer :teacher_id
      t.timestamps
    end
  end

  def self.down
    drop_table :announcements
  end
end

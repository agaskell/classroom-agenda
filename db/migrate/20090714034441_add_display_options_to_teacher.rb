class AddDisplayOptionsToTeacher < ActiveRecord::Migration
  def self.up
    add_column :teachers, :display_field_trips,   :boolean, :default => 1    
    add_column :teachers, :display_assignments,   :boolean, :default => 1
    add_column :teachers, :display_exams,         :boolean, :default => 1
    add_column :teachers, :display_announcements, :boolean, :default => 1
    add_column :teachers, :display_calendar,      :boolean, :default => 1     
  end

  def self.down
    remove_column :teachers, :display_field_trips
    remove_column :teachers, :display_assignments
    remove_column :teachers, :display_exams
    remove_column :teachers, :display_annoucements
    remove_column :teachers, :display_calendar
  end
end
class CreateAssignmentsClassrooms < ActiveRecord::Migration
  def self.up
    create_table :assignments_classrooms, :id => false do |t|
        t.integer :assignment_id
        t.integer :classroom_id
    end      
  end

  def self.down
    drop_table :assignments_classrooms
  end
end

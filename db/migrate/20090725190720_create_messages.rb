class CreateMessages < ActiveRecord::Migration
  def self.up
    create_table :messages do |t|
      t.integer :teacher_id
      t.string :email
      t.text :body
      t.string :ip_address
      t.boolean :read
      t.timestamps
    end
  end

  def self.down
    drop_table :messages
  end
end

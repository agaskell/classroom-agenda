class AddTeacherIdToClassroomGroup < ActiveRecord::Migration
  def self.up
    add_column :classroom_groups, :teacher_id, :integer
  end

  def self.down
    remove_column :classroom_groups, :teacher_id
    
  end
end

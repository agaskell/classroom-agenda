class AddLinkNameToAssets < ActiveRecord::Migration
  def self.up
    add_column :assets, :link_name, :string  
  end

  def self.down
    remove_column :assets, :link_name  
  end
end

class AddIndexes < ActiveRecord::Migration
  def self.up
    add_index 'announcements',          'teacher_id'  
    add_index 'assignments_classrooms', 'assignment_id'  
    add_index 'assignments_classrooms', 'classroom_id'
    add_index 'classroom_groups',       'teacher_id'
    add_index 'classrooms',             'classroom_group_id'
    add_index 'classrooms_exams',       'classroom_id'
    add_index 'classrooms_exams',       'exam_id'
    add_index 'classrooms_field_trips', 'classroom_id'
    add_index 'classrooms_field_trips', 'field_trip_id'
    add_index 'teachers',               'user_id'
  end

  def self.down
    remove_index 'announcements',           'teacher_id'  
    remove_index 'assignments_classrooms',  'assignment_id'  
    remove_index 'assignments_classrooms',  'classroom_id'
    remove_index 'classroom_groups',        'teacher_id'
    remove_index 'classrooms',              'classroom_group_id'
    remove_index 'classrooms_exams',        'classroom_id'
    remove_index 'classrooms_exams',        'exam_id'
    remove_index 'classrooms_field_trips',  'classroom_id'
    remove_index 'classrooms_field_trips',  'field_trip_id'
    remove_index 'teachers',                'user_id'
  end
end

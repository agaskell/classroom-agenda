class CreateClassroomGroups < ActiveRecord::Migration
  def self.up
    create_table :classroom_groups do |t|
      t.string :name
      t.boolean :is_active

      t.timestamps
    end
  end

  def self.down
    drop_table :classroom_groups
  end
end

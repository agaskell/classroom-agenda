class AddIsDemoToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :is_demo, :boolean, :default => 0     
  end

  def self.down
   remove_column :users, :is_demo
  end
end

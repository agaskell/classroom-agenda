class AddDateFieldsToAnnouncement < ActiveRecord::Migration
  def self.up
    add_column    :announcements, :start_date, :date
    add_column    :announcements, :end_date,   :date
    remove_column :announcements, :target_date
  end

  def self.down
    add_column    :announcements, :target_date, :date
    remove_column :announcements, :start_date
    remove_column :announcements, :end_date
  end
end

class CreateClassroomsFieldTrips < ActiveRecord::Migration
  def self.up
    create_table :classrooms_field_trips, :id => false do |t|
        t.integer :classroom_id
        t.integer :field_trip_id
    end
  end

  def self.down
    drop_table :classrooms_field_trips
  end
end

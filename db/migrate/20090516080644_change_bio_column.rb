class ChangeBioColumn < ActiveRecord::Migration
  def self.up
    change_column :teachers, :bio, :text 
  end

  def self.down
    change_column :teachers, :bio, :string 
  end
end

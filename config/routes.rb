ActionController::Routing::Routes.draw do |map|

  #map.resources :announcements, :controller => 'admin/announcements', :path_prefix => 'admin', :name_prefix => 'admin_'
  map.resources :announcements, :member => { :confirm_destroy => :get }

  #map.resources :assignments, :controller => 'admin/assignments', :path_prefix => 'admin', :name_prefix => 'admin_', :member => { :confirm_destroy => :get  }
  map.resources :assignments, :collection => { :all => :get }, :member => { :confirm_destroy => :get  }

  #map.resources :exams, :controller => 'admin/exams', :path_prefix => 'admin', :name_prefix => 'admin_', :member => { :confirm_destroy => :get }
  map.resources :exams, :collection => { :all => :get }, :member => { :confirm_destroy => :get }

  #map.resources :field_trips, :controller => 'admin/field_trips', :path_prefix => 'admin', :name_prefix => 'admin_', :member => { :confirm_destroy => :get }
  map.resources :field_trips, :collection => { :all => :get }, :member => { :confirm_destroy => :get }

  map.resources :teachers, :controller => 'admin/teachers', :path_prefix => 'admin', :name_prefix => 'admin_', :collection => { :dashboard => :get, :thanks => :get, :plans => :get, :billing => :any, :paypal => :any, :plan => :any, :plan_paypal => :any, :cancel => :any, :canceled => :get }
  map.resources :teachers, :only => [:show, :new, :create], :member => { :signup => :get, :signup_update => :put }, :collection => { :plans => :get, :export_agenda => :get, :search => :get }
  
  map.resources :users, :member => { :settings => :get, :admin => :put, :from_rpx => :get, :demo_menu => :get }, :collection => { :login_validate => :get, :rpx_create => :post, :demo => :get }
  map.resource :session, :collection => { :rpx_token => :post }
  
  map.resources :assets, :path_prefix => 'admin', :member => { :confirm_destroy => :get }, :collection =>  { :limit => :get }
  
  #subscriptions
  #map.with_options(:conditions => {:subdomain => /.+/}) do |subdom|
    #subdom.root :controller => 'subscription_admin/subscriptions', :action => 'index'
    #subdom.with_options(:namespace => 'subscription_admin/', :name_prefix => 'admin_', :path_prefix => nil) do |admin|
      #admin.resources :subscriptions, :member => { :charge => :post }
      #admin.resources :accounts
      #admin.resources :subscription_plans, :as => 'plans'
      #admin.resources :subscription_discounts, :as => 'discounts'
      #admin.resources :subscription_affiliates, :as => 'affiliates'
    #end
  #end

  map.resource :rpx, :only => [:create, :new], :controller => 'rpx'

  #classroom groups and classrooms
  map.resources :classroom_groups, :controller => 'admin/classroom_groups', :path_prefix => 'admin', :name_prefix => 'admin_', :new => { :signup => :get, :signup_create => :post }, :member => { :confirm_destroy => :get, :move_up => :put, :move_down => :put }, :collection => { :sort => :put, :needs_active_term => :get } do |classroom_group|
    classroom_group.resources :classrooms, :controller => 'admin/classrooms', :member => { :confirm_destroy => :get, :move_up => :put, :move_down => :put }, :collection => { :sort => :put }
  end
  
  map.resources :classroom_groups, :only => [:index] do |classroom_group|
    classroom_group.resources :classrooms, :only => [:index, :show]
  end
  
  map.resources :messages, :except => [:edit, :update], :member => { :confirm_destroy => :get }, :conditions => { :subdomain => true } 

  map.resources :classrooms, :only => [:index]

  map.teacher_root '', :controller => 'teachers', :action => 'show', :conditions => { :subdomain => true } 
  
  map.teacher_about '/about', :controller => 'teachers', :action => 'show', :conditions => { :subdomain => true } 
  map.teacher_feed '/feed', :controller => 'teachers', :action => 'show', :format => 'xml', :conditions => { :subdomain => true } 
  map.teacher_settings '/settings', :controller => 'users', :action => 'settings', :conditions => { :subdomain => true }
  
  map.connect '/:year/:month/:day',
              :conditions => { :subdomain => true, :method => :get }, 
              :controller => 'teachers', 
              :action => 'show_date', 
              :requirements => { :year => /(20)\d\d/, :month => /[01]\d/, :day => /[0-3]\d/ }

  
  #map.resources :users
  map.resource  :session,  :member => { :rpx_token, :any }
  
  map.forgot_password '/forgot', :controller => 'sessions', :action => 'forgot'
  map.reset_password '/reset/:token', :controller => 'sessions', :action => 'reset'
  
  #map.signup '/signup', :controller => 'users', :action => 'new'
  map.signup '/signup', :controller => 'rpx', :action => 'new' #:controller => 'teachers', :action => 'plans'
  map.new_user '/signup/:plan', :controller => 'users', :action => 'new', :plan => nil

  map.login  '/login', :controller => 'sessions', :action => 'new'
  map.logout '/logout', :controller => 'sessions', :action => 'destroy'    
  map.terms '/terms', :controller => 'home', :action => 'terms'
  map.students_and_parents '/students_and_parents', :controller => 'home', :action => 'students_and_parents'
  map.contact '/contact', :controller => 'home', :action => 'contact'
  map.about '/about', :controller => 'home', :action => 'about'
  map.demo '/demo', :controller => 'users', :action => 'demo'
  map.calendar_help '/add_to_calendar', :controller => 'help', :action => 'add_to_calendar'
  map.pricing '/pricing', :controller => 'home', :action => 'pricing'
  
  map.rpx_login_return '/login_return', :controller => 'rpx', :action => 'login_return'
  
  map.root :controller => 'home', :action => 'index'# :controller => "teachers", :action => "index"  
  map.root_js '.js', :controller => 'home', :action => 'index'
  #map.connect ':controller/:action/:id'
  #map.connect ':controller/:action/:id.:format'
end

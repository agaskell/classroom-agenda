class Date
  def tomorrow?
    self == ::Date.current.tomorrow
  end
end

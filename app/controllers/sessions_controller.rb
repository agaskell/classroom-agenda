# This controller handles the login/logout function of the site.  
class SessionsController < ApplicationController
  layout 'signup'
  protect_from_forgery :except => :rpx_token
  ssl_required :create, :new, :forgot, :reset, :destroy

  def create
    password_authentication(params[:login], params[:password])
  end

  def destroy
    self.current_user.forget_me if logged_in?
    cookies.delete :auth_token
    reset_session
    flash[:notice] = "You have been logged out."
    redirect_back_or_default('/')
  end
  
  def forgot
    return unless request.post?
    
    if !params[:email].blank? && @user = User.find_by_email(params[:email])
      PasswordReset.create(:user => @user, :remote_ip => request.remote_ip)
      render :action => 'forgot_complete'
    else
      flash[:error] = "That account wasn't found."
    end
  end
  
  def reset
    raise ActiveRecord::RecordNotFound unless @password_reset = PasswordReset.find_by_token(params[:token])
    raise ActiveRecord::RecordNotFound unless @password_reset.user.teacher.id == current_teacher.id
    
    if @password_reset.expired?
      @password_reset.destroy      
      flash[:error] = "Sorry, the password reset link is expired. You can request a new one."
      redirect_to forgot_password_url
    end
    
    @user = @password_reset.user
    return unless request.post?
    
    if !params[:user][:password].blank? && 
      if @user.update_attributes(:password => params[:user][:password],
        :password_confirmation => params[:user][:password_confirmation])
        @password_reset.destroy
        flash[:notice] = "Your password has been updated. Please log in with your new password."
        redirect_to new_session_url
      end
    end
  end  
  
  def new
    #render :layout => 'landing'
  end
  
  protected
  
    def open_id_authentication(openid_url)
      authenticate_with_open_id(openid_url, :required => [:nickname, :email]) do |result, identity_url, registration|
        if result.successful?
          @user = User.find_or_initialize_by_identity_url(identity_url)
          if @user.new_record?
            @user.login = registration['nickname']
            @user.email = registration['email']
            @user.save(false)
          end
          self.current_user = @user
          successful_login
        else
          failed_login result.message
        end
      end
    end
    
    def password_authentication(login, password)
      self.current_user = User.authenticate(login, password)
      if logged_in?
        successful_login
      else
        failed_login
      end
    end
    
    def failed_login(message = "Authentication failed.")
      flash.now[:error] = message
      render :action => 'new'
    end
    
    def successful_login
      if params[:remember_me] == "1"
        self.current_user.remember_me
        cookies[:auth_token] = { :value => self.current_user.remember_token , :expires => self.current_user.remember_token_expires_at }
      end
      #redirect_back_or_default('/')
      #redirect_to settings_user_path(current_user, :subdomain => current_user.login)  #teacher_root_url(:subdomain => current_user.login) + admin_user_path(current_user)
      redirect_to_settings
      flash[:notice] = "Logged in successfully as #{self.current_user.email}"
    end
  
end

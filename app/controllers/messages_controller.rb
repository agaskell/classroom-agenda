class MessagesController < ApplicationController
  before_filter :login_required, :except => [:new, :create]
  before_filter :logged_in_teacher, :except => [:new, :create]  
  before_filter :current_teacher, :only => [:new, :create]  
  
  def index
    @messages = logged_in_teacher.messages
  end

  def show
    @message = logged_in_teacher.messages.find(params[:id])
    @message.read = true
    @message.save

    respond_to do |format|
      format.html do |h|
        @unread_message_count = nil
        render :layout => 'admin'
      end
      format.js do |j|
        @unread_message_count = logged_in_teacher.unread_message_count
        render :partial => 'show'
      end
    end
  end

  def new
    @message = Message.new

    respond_to do |format|
      format.html
      format.js { render :partial => 'form', :locals => { :message => @message } }
    end
  end

  def create
    @message = current_teacher.messages.new(params[:message])    

    respond_to do |format|
      if @message.save
        flash[:notice] = 'Message was successfully sent.'
        format.html { redirect_to(teacher_root_url) }
        format.js  { render :nothing => true } #todo send a "thanks" message
      else
        format.html { render :action => "new" }
        format.js   { render :partial => 'form', :locals => { :message => @message } }
      end
    end
  end
  
  def confirm_destroy
    @message = logged_in_teacher.messages.find(params[:id]) 
    render :layout => 'admin'
  end

  def destroy
    @message = logged_in_teacher.messages.find(params[:id])
    @message.destroy
    flash[:notice] = 'Message was successfully deleted.'

    respond_to do |format|
      format.html { redirect_to(messages_url) }
      format.js do |j|
        @unread_message_count = logged_in_teacher.unread_message_count
        render :partial => "unread_message_count"
      end
    end
  end
end

# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  include AuthenticatedSystem
  include UrlHelper
  include ApplicationHelper
  include SslRequirement  
  before_filter :correct_safari_and_ie_accept_headers
  after_filter :set_xhr_flash
  
  helper :all # include all helpers, all the time

  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
  protect_from_forgery # :secret => 'a37c2b9427d5f0d5d3baff34055c74d0'
  
  # See ActionController::Base for details 
  # Uncomment this to filter the contents of submitted sensitive data parameters
  # from your application log (in this case, all fields with names like "password"). 
  filter_parameter_logging :password
  
  def ssl_required?
    return false if local_request?
    
    super
  end
  
protected
  
  def correct_safari_and_ie_accept_headers
    ajax_request_types = ['text/javascript', 'application/json', 'text/xml']
    request.accepts.sort!{ |x, y| ajax_request_types.include?(y.to_s) ? 1 : -1 } if request.xhr?
  end  
  
  def set_xhr_flash
    if request.xhr? || request.path.ends_with?(".js") 
      flash.discard
      #response.headers["Content-Type"] = "text/xml; charset=utf-8" if (request.put? || request.delete? || request.post?)
    end
  end
  
  def check_plan
    unless current_teacher.has_premium_plan?
      redirect_to root_url
    end
  end
  
  def check_subdomain
    #redirect_to billing logged_in_user.is_premium_
    if logged_in_teacher.has_premium_expired_trial?
      return if request.request_uri == billing_admin_teachers_path(:subdomain => current_user.login) || request.request_uri == plan_admin_teachers_path(:subdomain => current_user.login)
      redirect_to billing_admin_teachers_path(:subdomain => current_user.login) 
    end
    redirect_to_settings if current_subdomain.casecmp(current_user.login) != 0
  end

  def redirect_to_settings
    redirect_to settings_url 
  end
  
  def get_next_position(positionies)
    positionies.length == 0 ? 1 : positionies.last.position + 1
  end
  
  def rpx_setup
    unless Object.const_defined?(:API_KEY) && Object.const_defined?(:BASE_URL) && Object.const_defined?(:REALM)
      render :template => 'const_message'
      return false
    end
    @rpx = Rpx::RpxHelper.new(API_KEY, BASE_URL, REALM)
    return true
  end  
  
  def check_plan_admin
    logged_in_teacher
    unless @logged_in_teacher.has_premium_plan?
      flash[:error] = "You need to upgrade your Free plan to the Premium plan to access this feature"
      redirect_to plan_admin_teachers_url      
    end
  end
  
  def needs_classroom
    if logged_in_teacher.active_classroom_group.nil?
      redirect_to needs_active_term_admin_classroom_groups_path
    elsif logged_in_teacher.active_classrooms.length == 0
      flash[:error] = "You need at least one classroom to access this feature"
      redirect_to admin_classroom_group_classrooms_path(logged_in_teacher.active_classroom_group)
    end
  end  
  
  def teacher_required
    login_required
    logged_in_teacher
    check_subdomain        
  end
  
end

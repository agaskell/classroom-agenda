class FieldTripsController < ApplicationController
  before_filter :current_teacher
  #before_filter :check_plan, :only => [:index, :show, :all]
  
  before_filter :load_teacher_field_trip, :only => [:edit, :update, :destroy, :confirm_destroy]
  before_filter :teacher_required, :except => [:show, :index, :all]  
  
  def index
    @field_trips = current_teacher.upcoming_field_trips

    respond_to do |format|
      format.html
      format.js { render :partial => 'index', :layout => "partial_with_title" }
    end
  end

  def show
    @field_trip = current_teacher.all_field_trips.select { |a| a.id == params[:id].to_i }[0]
    
    respond_to do |format|
      format.html
      format.js { render(:partial => 'show', :layout => 'details') }      
    end
  end
  
  def all
    @field_trips = current_teacher.all_active_field_trips
    
    respond_to do |format|
      format.html
      format.js { render :partial => 'all', :layout => "partial_with_title" }
    end
  end    
  
  def new
    @field_trip = FieldTrip.new
  end

  def create
    @field_trip = FieldTrip.new(params[:field_trip])

    if @field_trip.save
      flash[:notice] = 'Field Trip was successfully created.'
      redirect_to field_trips_url
    else
      render :action => "new"
    end
  end

  def update
    if @field_trip.update_attributes(params[:field_trip])
      flash[:notice] = 'Field Trip was successfully updated.'
      redirect_to(field_trips_url)
    else
      render :action => "edit"
    end
  end

  def destroy
    @field_trip.destroy

    respond_to do |format|
      format.html { redirect_to(field_trips_url) }
      format.js  { render :nothing => true }
    end
  end
  
private
  def load_teacher_field_trip
    @field_trip = logged_in_teacher.all_active_field_trips.select { |e| e.id == params[:id].to_i }[0]
  end    
end

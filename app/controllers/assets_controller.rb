class AssetsController < ApplicationController
  before_filter :login_required
  before_filter :logged_in_teacher  

  before_filter :load_asset, :only => [:edit, :update, :destroy, :confirm_destroy]
  before_filter :check_attachment_count, :only => [:create, :new]
  
  layout 'admin'

  def index
    @assets = logged_in_teacher.assets

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @assignments }
    end
  end

  def show
    @assignment = Assignment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @assignment }
    end
  end

  def new
    @asset = Asset.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @asset }
    end
  end

  def create
    @asset = logged_in_teacher.assets.new(params[:asset]) 

    respond_to do |format|
      if @asset.save 
        #@logged_in_teacher.assets.push(@asset)
        update_attachings
        flash[:notice] = 'Attachment was successfully created.'
        format.html { redirect_to(assets_path) }
        format.xml  { render :xml => @asset, :status => :created, :location => @asset }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @asset.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    #params[:assignment][:classrooms] ||= []    
    #@asset = @logged_in_teacher.assets.find(params[:id])

    respond_to do |format|
      if @asset.update_attributes(params[:asset])
        update_attachings
        flash[:notice] = 'Attachment was successfully updated.'
        format.html { redirect_to(assets_path) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @asset.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def destroy
    @asset.destroy

    respond_to do |format|
      format.html { redirect_to(assets_path) }
      format.xml  { head :ok }
    end
  end
  
protected

  def check_attachment_count
    unless logged_in_teacher.has_premium_plan? && logged_in_teacher.assets.length >= 10
      render :action => "limit"
    end
  end

  def load_asset
    @asset ||= logged_in_teacher.assets.find(params[:id])
  end  
  
  def update_attachings
    @asset.attachings.clear
    @asset.attachings.push(Attaching.new { |a| a.attachable_type = @logged_in_teacher.class.name; a.attachable_id = @logged_in_teacher.id })
    
    params[:exam_ids].each { |e| @asset.attachings.push(Attaching.new { |a| a.attachable_type = Exam.name; a.attachable_id = e.to_i })} unless params[:exam_ids] == nil
    params[:assignment_ids].each { |e| @asset.attachings.push(Attaching.new { |a| a.attachable_type = Assignment.name; a.attachable_id = e.to_i })} unless params[:assignment_ids] == nil
    params[:field_trip_ids].each { |e| @asset.attachings.push(Attaching.new { |a| a.attachable_type = FieldTrip.name; a.attachable_id = e.to_i })} unless params[:field_trip_ids] == nil
    params[:announcement_ids].each { |e| @asset.attachings.push(Attaching.new { |a| a.attachable_type = Announcement.name; a.attachable_id = e.to_i })} unless params[:announcement_ids] == nil
  end
end

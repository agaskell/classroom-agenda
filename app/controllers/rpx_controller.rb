require 'evri_rpx'

class RpxController < ApplicationController
  protect_from_forgery :except => :create
  ssl_allowed :create
  layout 'signup'

  def create
    if params[:token].blank?
      flash[:notice] = 'You cancelled login.'
      redirect_to root_url
      return
    end

    rpx_user = rpx.auth_info(params[:token])
    u = User.find_by_identifier(rpx_user.identifier)
    
    #if they are a returning user
    if !u.nil?
      self.current_user = u
      redirect_to settings_url 
    else 
      #try to match on email
      u = User.find_by_email(rpx_user.email) 
      if !u.nil? 
        self.current_user = u
        u.identifier = rpx_user.identifier
        u.save!
        redirect_to settings_url 
      else
        #sign up a new user from rpx
        sanitized_username = rpx_user.username.nil? ? nil : rpx_user.username.gsub(/[^a-z0-9]+/i, '_') 
        username = User.find_by_login(sanitized_username).nil? ? sanitized_username : nil
        #self.current_user = User.create!({ :identifier => rpx_user.identifier, :email => rpx_user.email, :login => username, :photo_url => rpx_user.photo, :full_name => rpx_user.display_name })
        #User.create(data)
        self.rpx_user = User.new do |u| 
          u.identifier = rpx_user.identifier 
          u.email = rpx_user.email 
          u.login = username
          u.photo_url = rpx_user.photo
          u.full_name = rpx_user.name
        end
        
        redirect_to plans_teachers_url
      end
    end      
  end

  private
    def rpx
      @rpx ||= Evri::RPX::Session.new('40b60202cef47e84e1d1a34a8cdb27cbf08d322f')
    end
end


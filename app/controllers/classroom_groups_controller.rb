class ClassroomGroupsController < ApplicationController
  before_filter :current_teacher

  def index
    @classroom_groups = current_teacher.classroom_groups
  end
  
end

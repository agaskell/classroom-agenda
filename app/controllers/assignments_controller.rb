class AssignmentsController < ApplicationController
  before_filter :current_teacher
  
  before_filter :load_teacher_assignment, :only => [:edit, :update, :destroy, :confirm_destroy]
  before_filter :teacher_required, :except => [:show, :index, :all]

  def index
    @assignments = current_teacher.open_assignments

    respond_to do |format|
      format.html
      format.js { render(:partial => 'index', :layout => "partial_with_title") }
    end
  end

  def show
    @assignment = current_teacher.all_assignments.select { |a| a.id == params[:id].to_i }[0]

    respond_to do |format|
      format.html
      format.js { render(:partial => 'show', :layout => 'details') }
    end
  end

  def all
    @assignments = current_teacher.all_active_assignments
    
    respond_to do |format|
      format.html
      format.js { render(:partial => 'all', :layout => "partial_with_title") }
    end
  end

  def new
    @assignment = Assignment.new
  end

  def create
    @assignment = Assignment.new(params[:assignment]) 

    if @assignment.save
      flash[:notice] = 'Assignment was successfully created.'
      redirect_to assignments_url
    else
      render :action => "new"
    end
  end

  def update
    if @assignment.update_attributes(params[:assignment])
      flash[:notice] = 'Assignment was successfully updated.'
      redirect_to(assignments_url) 
    else
      render :action => "edit"
    end
  end

  def destroy
    @assignment.destroy

    respond_to do |format|
      format.html { redirect_to(assignments_url) }
      format.js  { render :nothing => true }
    end
  end

protected
  def load_teacher_assignment
    @assignment = logged_in_teacher.all_active_assignments.select { |e| e.id == params[:id].to_i }[0]
  end
  
end

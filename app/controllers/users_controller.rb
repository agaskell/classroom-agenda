class UsersController < ApplicationController 
  before_filter :login_required, :only => [:edit, :update, :settings]
  before_filter :logged_in_teacher, :only => [:edit, :update, :settings]
  before_filter :check_subdomain, :only => [:edit, :update, :settings]
  before_filter :build_user, :only => [:create]
  before_filter :build_rpx_user, :only => [:rpx_create]
  before_filter :build_plan, :only => [:create, :rpx_create]
  before_filter :load_billing, :only => [:create, :rpx_create]
  before_filter :build_payment_info, :only => [:create, :rpx_create]  
  layout 'admin', :only => [:settings, :edit]

  ssl_required :new, :edit, :create
  ssl_allowed  :login_validate, :rpx_create

  def new
    @user = rpx_user
    if @user.nil?
      @user = User.new
      render :layout => 'signup'
    else
      render :action => :from_rpx, :layout => 'signup'
    end
  end
  
  def rpx_create
    if @user.teacher.save && @user.save
      self.current_user = @user
      redirect_to signup_teacher_path(@user.teacher, :subdomain => @user.login)
    else
      render :layout => 'signup', :action => 'from_rpx'
    end
  end
  
  def update
    @user = current_user
    
    if @user.update_attributes(params[:user])
      flash[:notice] = 'User settings successfully updated.'
      redirect_to_settings
    else
      render :action => "edit"
    end
  end
  
  def edit
    @user = current_user
  end
  
  def create
    cookies.delete :auth_token
    # protects against session fixation attacks, wreaks havoc with 
    # request forgery protection.
    # uncomment at your own risk
    # reset_session
    
    if @user.teacher.save && @user.save
      self.current_user = @user
      redirect_to signup_teacher_path(@user.teacher, :subdomain => @user.login)
      #flash[:notice] = "Thanks for signing up!"
    else
      render :action => 'new', :layout => 'signup'
    end
  end
  
  def login_validate
    @user = User.new(params[:user])
    @user.valid?
    
    @user = current_user if !current_user.nil? && @user.login == current_user.login
    
    respond_to do |format|
      format.js  { render :partial => "login_validate", :locals => { :errors => @user.errors["login"] } }
    end
  end
  
  def demo
    self.current_user = User.create_demo_user
    render :layout => false
  end
  
  protected

    def build_rpx_user
      @user = rpx_user
      @user.first_name = params[:user][:first_name]
      @user.last_name = params[:user][:last_name]
      @user.login = params[:user][:login]
      @user.email = params[:user][:email]
      @user.build_teacher
      logger.info("photo_url = #{@user.photo_url}")
      @user.teacher.download_remote_image(@user.photo_url) unless @user.photo_url.nil?
    end
  
    def build_user
      @user = User.new(params[:user])
      @user.build_teacher
    end
  
    def build_plan
      redirect_to :action => "plans" unless @plan = SubscriptionPlan.find_by_name(params[:plan])
      @user.teacher.plan = @plan
    end   
    
    def load_billing
      @creditcard = ActiveMerchant::Billing::CreditCard.new(params[:creditcard])
      @address = SubscriptionAddress.new(params[:address])
    end     

    def build_payment_info
      if @user.teacher.needs_payment_info?
        @address.first_name = @creditcard.first_name
        @address.last_name = @creditcard.last_name
        @user.teacher.address = @address
        @user.teacher.creditcard = @creditcard      
      end    
    end

end

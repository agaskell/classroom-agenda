require 'ri_cal'

class TeachersController < ApplicationController
  before_filter :current_teacher, :except => [:signup, :signup_update, :create, :plans, :search]
  ssl_required :signup, :signup_update, :create, :plans
  
  def about 
    show
  end

  def show
    @classrooms = current_teacher.active_classrooms

    @todays_assignments = current_teacher.todays_assignments
    @tomorrows_assignments = current_teacher.tomorrows_assignments
    
    @todays_field_trips = current_teacher.todays_field_trips
    @tomorrows_field_trips = current_teacher.tomorrows_field_trips

    @todays_exams = current_teacher.todays_exams
    @tomorrows_exams = current_teacher.tomorrows_exams

    @todays_feed = current_teacher.date_feed

    respond_to do |format|
      format.html 
      format.js { render(:partial => 'show') }  
      format.xml { render :action => 'feed.atom', :layout => false  } 
    end
  end

  def new
    @teacher = Teacher.new
  end

  def signup
    @teacher = current_user.teacher
    render :layout => 'signup'
  end
  
  def plans
    @plans = SubscriptionPlan.find(:all, :order => 'amount desc') 
    render :layout => 'signup'
  end  
  
  def signup_update
    @teacher = current_user.teacher
    
    if @teacher.update_attributes(params[:teacher])
      redirect_to signup_new_admin_classroom_group_path
    else
      render :action => "signup"
    end
  end

  def create
    @teacher = Teacher.new(params[:teacher])
    
    if @teacher.needs_payment_info?
      @teacher.first_name = @creditcard.first_name
      @teacher.last_name = @creditcard.last_name
      @teacher.address = @address
      @teacher.creditcard = @creditcard
    end

    if @teacher.save
      flash[:notice] = 'Teacher was successfully created.'
      redirect_to teacher_path(@teacher)
    else
      render :action => "new"
    end
  end
  
  def export_agenda 
    current_teacher.calendar_settings = CalendarSettings.new(params)

    respond_to do |format|
      format.ics do
        cal = RiCal.Calendar do |cal|
          current_teacher.all_announcements.select{ |a| !a.start_date.nil? && !a.end_date.nil? }.each do |a|
            cal.event do |e|
              e.summary = "Announcement: #{a.title}"
              e.description = a.details
              e.dtstart = a.start_date
              e.dtend = a.end_date + 1.day
            end
          end
        
          current_teacher.all_assignments.select{ |a| !a.start_date.nil? && !a.due_date.nil? }.each do |a|
            cal.event do |e|
              e.summary = "Assignment: #{a.title}"
              e.description = a.details
              e.dtstart = a.start_date
              e.dtend = a.due_date + 1.day
            end
          end
        
          (current_teacher.all_exams + current_teacher.all_field_trips).select{ |a| !a.target_date.nil? }.each do |a|
            cal.event do |e|
              e.summary = "#{a.class.table_name.humanize.singularize}: #{a.title}" 
              e.description = a.details
              e.dtend = e.dtstart = a.target_date
            end
          end
        end
        
        render :text => cal.export
      end
    end 
  end
  
  def show_date
    @teacher = current_teacher
    
    @date = params[:year].nil? ? Date.current : Date.new(params[:year].to_i, params[:month].to_i, params[:day].to_i)
    
    @calendar_settings = current_teacher.calendar_settings = CalendarSettings.new(params)

    @date_assignments = @teacher.assignments_by_date(@date)
    @date_field_trips = @teacher.field_trips_by_date(@date)
    @date_exams = @teacher.exams_by_date(@date)

    @date_feed = @teacher.date_feed(@date)
  
    respond_to do |format|
      format.html { render 'show_date' }
      format.js { render(:partial => 'show_date', :layout => "with_content_for") }
    end
  end
  
  def search
    @teachers = Teacher.user_last_name_like(params[:q]).user_is_demo_eq(false)
    render :layout => 'signup'
  end

end

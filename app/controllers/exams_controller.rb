class ExamsController < ApplicationController
  before_filter :current_teacher
  #before_filter :check_plan, :only => [:index, :show, :all]
  
  before_filter :load_teacher_exam, :only => [:edit, :update, :destroy, :confirm_destroy]
  before_filter :teacher_required, :except => [:show, :index, :all]
  
  def index
    @exams = current_teacher.upcoming_exams

    respond_to do |format|
      format.html
      format.js { render :partial => 'index', :layout => "partial_with_title" }
    end
  end

  def show    
    @exam = current_teacher.all_active_exams.select { |e| e.id == params[:id].to_i }[0]    
    
    respond_to do |format|
      format.html
      format.js { render :partial => 'show', :layout => 'details' }
    end
  end
  
  def all
    @exams = current_teacher.all_active_exams
    
    respond_to do |format|
      format.html
      format.js { render :partial => 'all', :layout => "partial_with_title" }
    end
  end  
  
   def new
     @exam = Exam.new
   end

   def create
     @exam = Exam.new(params[:exam])

     if @exam.save
       flash[:notice] = 'Exam was successfully created.'
       redirect_to(exams_url)
     else
       render :action => "new"
     end
  end  
  
  def update
    respond_to do |format|
      if @exam.update_attributes(params[:exam])
        flash[:notice] = 'Exam was successfully updated.'
        format.html { redirect_to(exams_url) }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @exam.destroy

    respond_to do |format|
      format.html { redirect_to(exams_url) }
      format.js  { render :nothing => true }
    end
  end  

private
  def load_teacher_exam
    @exam = logged_in_teacher.all_active_exams.select { |e| e.id == params[:id].to_i }[0]
  end
  
end

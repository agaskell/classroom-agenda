class ClassroomsController < ApplicationController
  before_filter :current_teacher

  def index
    @classrooms = current_teacher.active_classrooms

    respond_to do |format|
      format.html
      format.js { render :partial => 'index' }      
    end
  end 
  
  def show
    @classroom = current_teacher.active_classrooms.select { |a| a.id == params[:id].to_i }[0]
    
    @classrooms = current_teacher.active_classrooms

    @todays_assignments = filter_events current_teacher.todays_assignments
    
    @tomorrows_assignments = filter_events current_teacher.tomorrows_assignments
    
    @todays_field_trips = filter_events current_teacher.todays_field_trips
    @tomorrows_field_trips = filter_events current_teacher.tomorrows_field_trips

    @todays_exams = filter_events current_teacher.todays_exams
    @tomorrows_exams = filter_events current_teacher.tomorrows_exams

    @todays_feed = filter_events current_teacher.date_feed

    respond_to do |format|
      format.html
      format.js { render(:partial => 'show') }  
    end  
  end

  private 
    def filter_events(events)
      events.select { |a| !a.respond_to?('classroom_ids') || a.classroom_ids.include?(@classroom.id) }
    end

end

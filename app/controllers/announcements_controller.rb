class AnnouncementsController < ApplicationController
  before_filter :current_teacher
  #before_filter :check_plan, :only => [:index, :show, :all]
  
  before_filter :load_teacher_announcement, :only => [:edit, :update, :destroy, :confirm_destroy]
  before_filter :teacher_required, :except => [:show, :index, :all]    
  
  def index
    @announcements = current_teacher.announcements
  end

  def show
    @announcement = current_teacher.announcements.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.js { render(:partial => 'show', :layout => 'details') }           
    end
  end
  
  def new
    @announcement = Announcement.new
  end

  def create
    @announcement = logged_in_teacher.announcements.new(params[:announcement])    

    if @announcement.save
      flash[:notice] = 'Announcement was successfully created.'
      redirect_to announcements_url
    else
      render :action => "new"
    end
  end

  def update
    if @announcement.update_attributes(params[:announcement])
      flash[:notice] = 'Announcement was successfully updated.'
      redirect_to announcements_url
    else
      render :action => "edit"
    end
  end

  def destroy
    @announcement.destroy

    respond_to do |format|
      format.html { redirect_to(announcements_url) }
      format.js  { render :nothing => true }
    end
  end

private
  def load_teacher_announcement
    @announcement = logged_in_teacher.announcements.find(params[:id])
  end  

end

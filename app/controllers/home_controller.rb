class HomeController < ApplicationController
  layout 'signup'
  
  def index
    render :layout => 'landing'
  end
  
  def contact
    if request.post?
      unless params[:body].blank?
        SubscriptionNotifier.deliver_contact(params[:email], params[:body])
        render :thanks #, :layout => 'landing'
      end
    end
  end
  
  def students_and_parents
    if request.post?
      unless params[:body].blank?
        SubscriptionNotifier.deliver_contact_teacher(params[:email], params[:name], params[:body])
        render :teacher_contact_thanks
      end      
    end
  end
  
  def pricing
  end
end

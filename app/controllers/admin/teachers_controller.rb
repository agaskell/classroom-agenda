class Admin::TeachersController < Admin::AdminController
  before_filter :load_billing, :only => [ :new, :create, :billing, :paypal ]
  before_filter :load_subscription, :only => [ :billing, :plan, :paypal, :plan_paypal ]
  skip_before_filter :check_plan_admin 
  
  ssl_required :billing

  def billing
    if request.post?
      if params[:paypal].blank?
        @address.first_name = @creditcard.first_name
        @address.last_name = @creditcard.last_name
        if @creditcard.valid? & @address.valid?
          if @subscription.store_card(@creditcard, :billing_address => @address.to_activemerchant, :ip => request.remote_ip)
            flash[:notice] = "Your billing information has been updated."
            redirect_to :action => "billing"
          end
        end
      else
        if redirect_url = @subscription.start_paypal(paypal_admin_teachers_url, billing_admin_teachers_url)
          redirect_to redirect_url
        end
      end
    end
  end
  
  # Handle the redirect return from PayPal
  def paypal
    if params[:token]
      if @subscription.complete_paypal(params[:token])
        flash[:notice] = 'Your billing information has been updated'
        redirect_to :action => "billing"
      else
        render :action => 'billing'
      end
    else
      redirect_to :action => "billing"
    end
  end
  
  def plan
    if request.post?
      @subscription.plan = SubscriptionPlan.find(params[:plan_id])

      # PayPal subscriptions must get redirected to PayPal when
      # changing the plan because a new recurring profile needs
      # to be set up with the new charge amount.
      if @subscription.paypal?
        # Purge the existing payment profile if the selected plan is free
        if @subscription.amount == 0
          logger.info "FREE"
          if @subscription.purge_paypal
            logger.info "PAYPAL"
            flash[:notice] = "Your subscription has been changed to the #{@subscription.plan} plan."
            SubscriptionNotifier.deliver_plan_changed(@subscription)
          else
            flash[:error] = "Error deleting PayPal profile: #{@subscription.errors.full_messages.to_sentence}"
          end
          redirect_to settings_url and return
        else
          if redirect_url = @subscription.start_paypal(plan_paypal_account_url(:plan_id => params[:plan_id]), plan_account_url)
            redirect_to redirect_url and return
          else
            flash[:error] = @subscription.errors.full_messages.to_sentence
            redirect_to :action => "plan" and return
          end
        end
      end
      
      if @subscription.save
        flash[:notice] = "Your subscription has been changed to the #{SubscriptionPlan.find(params[:plan_id]).name} plan."
        if @subscription.needs_payment_info?
          flash[:notice] += " Please make sure you %s."
          flash[:notice_item] = ['enter your billing information', billing_admin_teachers_url]
        end
        SubscriptionNotifier.deliver_plan_changed(@subscription)
      else
        flash[:error] = "Error updating your plan: #{@subscription.errors.full_messages.to_sentence}"
      end
      redirect_to settings_url
    else
      @plan = SubscriptionPlan.find(:first, :conditions => ['id <> ?', @subscription.subscription_plan_id], :order => 'amount desc') #.collect {|p| p.discount = @subscription.discount; p }
    end
  end    
  
  # Handle the redirect return from PayPal when changing plans
  def plan_paypal
    if params[:token]
      @subscription.plan = SubscriptionPlan.find(params[:plan_id])
      if @subscription.complete_paypal(params[:token])
        flash[:notice] = "Your subscription has been changed to the #{@subscription.plan} plan."
        SubscriptionNotifier.deliver_plan_changed(@subscription)
        redirect_to :action => "plan"
      else
        flash[:error] = "Error completing PayPal profile: #{@subscription.errors.full_messages.to_sentence}"
        redirect_to :action => "plan"
      end
    else
      redirect_to :action => "plan"
    end
  end  
     
  # GET /teachers/1/edit
  def edit
    @teacher = logged_in_teacher  #Teacher.find(params[:id])
    
    respond_to do |format|
      format.html { render :layout => 'admin' }
      format.js { render :layout => 'layouts/boxy' }
    end  
  end
  
  # PUT /teachers/1
  # PUT /teachers/1.xml
  def update
    @teacher = logged_in_teacher  #Teacher.find(params[:id])

    respond_to do |format|
      if @teacher.update_attributes(params[:teacher])
        flash[:notice] = 'Teacher settings were successfully updated.'
        format.html { redirect_to_settings }
        format.xml  { head :ok }
        format.js  { render :nothing => true }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @teacher.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def signup_plan
    plan
    
    #respond_to do |format|
    #  format.html 
    #end
  end

private
  def load_billing
    @creditcard = ActiveMerchant::Billing::CreditCard.new(params[:creditcard])
    @address = SubscriptionAddress.new(params[:address])
  end

  def load_subscription
    @subscription = logged_in_teacher.subscription
  end  

end

class Admin::AdminController < ApplicationController
  before_filter :login_required
  before_filter :check_plan_admin
  before_filter :logged_in_teacher  
  before_filter :check_subdomain
  layout 'admin'
  


end



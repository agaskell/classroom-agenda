class Admin::ClassroomsController < Admin::AdminController
  skip_before_filter :check_plan_admin
  before_filter :load_classroom, :only => [:edit, :update, :confirm_destroy, :destroy, :move_up, :move_down]
  
  def index
    @classroom_group = logged_in_teacher.classroom_groups.find(params[:classroom_group_id])
    @classrooms = @classroom_group.classrooms
  end

  def new
    classroom_group = logged_in_teacher.classroom_groups.find(params[:classroom_group_id])
    @classroom = classroom_group.classrooms.build
    @classroom.display = true #active by default

    respond_to do |format|
      format.html
      format.js { render :partial => 'form' }
    end
  end

  def create
    classroom_group = logged_in_teacher.classroom_groups.find(params[:classroom_group_id])    
    #need to fetch the next display order before the call to build
    position = get_next_position(classroom_group.classrooms)
    @classroom = classroom_group.classrooms.build(params[:classroom])
    @classroom.position = position

    respond_to do |format|
      if @classroom.save
        flash[:notice] = 'Classroom was successfully created.'
        format.html { redirect_to admin_classroom_group_classrooms_path }
        format.js  { render :partial => "row", :locals => { :classroom => @classroom } }
      else
        format.html { render :action => "new" }
        format.js { render :partial => 'form' }
      end
    end
  end

  def edit
    respond_to do |format|
      format.html
      format.js { render :partial => 'form', :locals => { :classroom => @classroom } }
    end
  end

  def update
    respond_to do |format|
      if @classroom.update_attributes(params[:classroom])
        flash[:notice] = 'Classroom was successfully updated.'
        format.html { redirect_to admin_classroom_group_classrooms_path }
        format.js  { render :partial => "row", :locals => { :classroom => @classroom } }
      else
        format.html { render :action => "edit" }
        format.js { render :partial => 'form' }
      end
    end
  end

  def destroy
    @classroom.destroy

    respond_to do |format|
      format.html { redirect_to(admin_classroom_group_classrooms_path(@classroom_group)) }
      format.js  { render :nothing => true }
    end
  end
  
  def move_up
    @classroom.move_higher
    redirect_to admin_classroom_group_classrooms_path
  end
  
  def move_down
    @classroom.move_lower
    redirect_to admin_classroom_group_classrooms_path
  end
  
  def sort
    classroom_group = logged_in_teacher.classroom_groups.find(params[:classroom_group_id])    
    classroom_group.classrooms.each do |c|
      c.position = params["row"].index(c.id.to_s) + 1
      c.save
    end
        
    render :nothing => true
  end

private
  def load_classroom
    @classroom_group = logged_in_teacher.classroom_groups.find(params[:classroom_group_id])
    @classroom = @classroom_group.classrooms.find(params[:id])
  end
end

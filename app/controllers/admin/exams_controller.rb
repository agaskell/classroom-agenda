class Admin::ExamsController < Admin::AdminController
  before_filter :load_exam, :only => [:show, :edit, :update, :destroy, :confirm_destroy]
  before_filter :needs_classroom, :only => [:index]
  
  def index
    @exams = logged_in_teacher.all_active_exams
  end

  def new
    @exam = Exam.new
  end

  def create
    @exam = Exam.new(params[:exam])

    if @exam.save
      flash[:notice] = 'Exam was successfully created.'
      redirect_to(admin_exams_url)
    else
      render :action => "new"
    end
 end

  def update
    respond_to do |format|
      if @exam.update_attributes(params[:exam])
        flash[:notice] = 'Exam was successfully updated.'
        format.html { redirect_to(admin_exams_url) }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @exam.destroy

    respond_to do |format|
      format.html { redirect_to(admin_exams_url) }
      format.js  { render :nothing => true }
    end
  end
  
private
  def load_exam
    @exam = logged_in_teacher.all_active_exams.select { |e| e.id == params[:id].to_i }[0]
  end
end

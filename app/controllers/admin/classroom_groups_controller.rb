class Admin::ClassroomGroupsController < Admin::AdminController
  before_filter :load_classroom_group, :only => [:edit, :update, :confirm_destroy, :destroy, :move_up, :move_down]
  after_filter :clean_up_active, :only => [ :update, :create ]
  skip_before_filter :check_plan_admin  
  ssl_required :signup, :signup_create
    
  def index
    @classroom_groups = logged_in_teacher.classroom_groups
  end
  
  def new
    @classroom_group = logged_in_teacher.classroom_groups.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.js { render :partial => 'form', :locals => { :classroom_group => @classroom_group } }
    end
  end
  
  def needs_active_term
    flash[:error] = 'You need to have an active term before accessing this feature'
    redirect_to admin_classroom_groups_path
  end
  
  def create
    @classroom_group = logged_in_teacher.classroom_groups.new(params[:classroom_group])
    @classroom_group.position =  get_next_position(logged_in_teacher.classroom_groups)

    respond_to do |format|
      if @classroom_group.save
        flash[:notice] = 'Term was successfully created.'
        format.html { redirect_to admin_classroom_groups_path }
        format.js   { render :partial => 'row', :locals => { :classroom_group => @classroom_group } }
      else
        format.html { render :action => "new" }
        format.js   { render :partial => 'form', :locals => { :classroom_group => @classroom_group } }
      end
    end  
  end
  
  def edit
    respond_to do |format|
      format.html # show.html.erb
      format.js { render :partial => 'form', :locals => { :classroom_group => @classroom_group } }
    end
  end
  
  def update
    respond_to do |format|
      if @classroom_group.update_attributes(params[:classroom_group])
        flash[:notice] = 'Term was successfully updated.'
        format.html { redirect_to admin_classroom_groups_path }
        format.js  { render :partial => "row", :locals => { :classroom_group => @classroom_group } }
      else
        format.html { render :action => "edit" }
        format.js   { render :partial => 'form', :locals => { :classroom_group => @classroom_group } }
      end
    end  
  end
  
  def destroy
    @classroom_group.destroy

    respond_to do |format|
      format.html { redirect_to(admin_classroom_groups_url) }
      format.js  { render :nothing => true }
    end
  end
  
  def move_up
    @classroom_group.move_higher
    redirect_to admin_classroom_groups_path
  end
  
  def move_down
    @classroom_group.move_lower
    redirect_to admin_classroom_groups_path
  end
  
  def signup
    @classroom_group = logged_in_teacher.classroom_groups.new

    5.times { @classroom_group.classrooms.build }
    
    respond_to do |format|
      format.html { render :layout => 'signup' }
    end
  end
  
  def signup_create
    @classroom_group = logged_in_teacher.classroom_groups.new(params[:classroom_group])
    @classroom_group.position =  get_next_position(@logged_in_teacher.classroom_groups)
    @classroom_group.is_active = true

    respond_to do |format|
      if @classroom_group.save
        rpx_user = nil #we're done with registration - if we have rpx data in session, remove it.
        flash[:notice] = 'Signup complete!'
        format.html { redirect_to_settings }
      else
        format.html do 
          (5 - @classroom_group.classrooms.length).times { @classroom_group.classrooms.build }
          render :action => "signup", :layout => 'signup'
        end
      end
    end    
  end

  def sort
    logged_in_teacher.classroom_groups.each do |c|
      c.position = params["row"].index(c.id.to_s) + 1
      c.save
    end
        
    render :nothing => true
  end  
  
private

  def clean_up_active
    classroom_groups = logged_in_teacher.classroom_groups.find(:all, :conditions => {:is_active => true}, :order => "updated_at")
    
    if(!classroom_groups.nil?)
      for i in 0..classroom_groups.length - 2
        classroom_groups[i].is_active = false
        classroom_groups[i].save!
      end
    end
  end
  
  def load_classroom_group
    @classroom_group = logged_in_teacher.classroom_groups.find(params[:id])    
  end
  
end

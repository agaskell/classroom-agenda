class Admin::AnnouncementsController < Admin::AdminController
  before_filter :load_announcement, :except => [:index, :new, :create]
  
  def index
    @announcements = logged_in_teacher.announcements
  end

  def new
    @announcement = Announcement.new
  end

  def create
    @announcement = logged_in_teacher.announcements.new(params[:announcement])    

    if @announcement.save
      flash[:notice] = 'Announcement was successfully created.'
      redirect_to admin_announcements_url
    else
      render :action => "new"
    end
  end

  def update
    if @announcement.update_attributes(params[:announcement])
      flash[:notice] = 'Announcement was successfully updated.'
      redirect_to admin_announcements_url
    else
      render :action => "edit"
    end
  end

  def destroy
    @announcement.destroy

    respond_to do |format|
      format.html { redirect_to(admin_announcements_url) }
      format.js  { render :nothing => true }
    end
  end
  
protected
  def load_announcement
    @announcement = logged_in_teacher.announcements.find(params[:id])
  end 
end

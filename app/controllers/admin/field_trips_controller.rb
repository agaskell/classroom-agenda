class Admin::FieldTripsController < Admin::AdminController
  before_filter :load_field_trip, :only => [:show, :edit, :update, :destroy, :confirm_destroy]
  before_filter :needs_classroom, :only => [:index]
  
  def index
    @field_trips = logged_in_teacher.all_active_field_trips
  end

  def new
    @field_trip = FieldTrip.new
  end

  def create
    @field_trip = FieldTrip.new(params[:field_trip])

    if @field_trip.save
      flash[:notice] = 'Field Trip was successfully created.'
      redirect_to admin_field_trips_url
    else
      render :action => "new"
    end
  end

  def update
    if @field_trip.update_attributes(params[:field_trip])
      flash[:notice] = 'Field Trip was successfully updated.'
      redirect_to(admin_field_trips_url)
    else
      render :action => "edit"
    end
  end

  def destroy
    @field_trip.destroy

    respond_to do |format|
      format.html { redirect_to(admin_field_trips_url) }
      format.js  { render :nothing => true }
    end
  end
  
private
  def load_field_trip
    @field_trip = logged_in_teacher.all_active_field_trips.select { |e| e.id == params[:id].to_i }[0]
  end  
end

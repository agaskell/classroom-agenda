class Admin::AssignmentsController < Admin::AdminController
  before_filter :load_assignment, :only => [:edit, :update, :destroy, :confirm_destroy, :show]
  before_filter :needs_classroom, :only => [:index]
  skip_before_filter :check_plan_admin

  def index
    @assignments = logged_in_teacher.all_active_assignments
  end

  def new
    @assignment = Assignment.new
  end

  def create
    @assignment = Assignment.new(params[:assignment]) 

    if @assignment.save
      flash[:notice] = 'Assignment was successfully created.'
      redirect_to admin_assignments_url
    else
      render :action => "new"
    end
  end

  def update
    if @assignment.update_attributes(params[:assignment])
      flash[:notice] = 'Assignment was successfully updated.'
      redirect_to(admin_assignments_url) 
    else
      render :action => "edit"
    end
  end
  
  def destroy
    @assignment.destroy

    respond_to do |format|
      format.html { redirect_to(admin_assignments_url) }
      format.js  { render :nothing => true }
    end
  end
  
protected
  def load_assignment
    @assignment = logged_in_teacher.all_active_assignments.select { |e| e.id == params[:id].to_i }[0]
  end  
end

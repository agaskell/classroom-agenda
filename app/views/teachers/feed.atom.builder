atom_feed do |feed|
  feed.title "#{current_teacher.salutation}'s feed"
  feed.updated DateTime.now

  (@todays_feed + @todays_assignments + @todays_exams + @todays_field_trips ).each do |a|
    feed.entry(a) do |entry|
      entry.title(a.title)
      entry.content(a.details)
      entry.author do |author|
        author.name(current_teacher.salutation)
      end
    end
  end

end


class SubscriptionNotifier < ActionMailer::Base
  include ActionView::Helpers::NumberHelper
  
  def setup_email(to, subject, from = AppConfig['from_email'])
    @sent_on = Time.now
    @subject = subject
    @recipients = to.respond_to?(:email) ? to.email : to
    @from = from.respond_to?(:email) ? from.email : from
  end
  
  def welcome(user)
    setup_email(user, "Welcome to #{AppConfig['app_name']}!")
    @body = { :teacher => user.teacher, :user => user }
  end
  
  def trial_expiring(subscription)
    setup_email(subscription.teacher.user, 'Trial period expiring')
    @body = { :user => subscription.teacher.user, :subscription => subscription }
  end
  
  def charge_receipt(subscription_payment)
    setup_email(subscription_payment.subscription.teacher.user, "Your #{AppConfig['app_name']} invoice")
    @body = { :subscription => subscription_payment.subscription, :amount => subscription_payment.amount }
  end
  
  def setup_receipt(subscription_payment)
    setup_email(subscription_payment.subscription.teacher.user, "Your #{AppConfig['app_name']} invoice")
    @body = { :subscription => subscription_payment.subscription, :amount => subscription_payment.amount }
  end
  
  def misc_receipt(subscription_payment)
    setup_email(subscription_payment.subscription.teacher.user, "Your #{AppConfig['app_name']} invoice")
    @body = { :subscription => subscription_payment.subscription, :amount => subscription_payment.amount }
  end
  
  def charge_failure(subscription)
    setup_email(subscription.teacher.user, "Your #{AppConfig['app_name']} renewal failed")
    @bcc = AppConfig['from_email']
    @body = { :subscription => subscription }    
  end
  
  def plan_changed(subscription)
    setup_email(subscription.teacher.user, "Your #{AppConfig['app_name']} plan has been changed")
    @body = { :subscription => subscription }    
  end
  
  def password_reset(reset)
    setup_email(reset.user, 'Password Reset Request')
    @body = { :reset => reset }
  end
  
  def message_notification(message)
    setup_email(message.teacher.user, "You received a new message on #{AppConfig['app_name']}")
    @body = { :message => message }
  end
  
  def contact(email, body)
    setup_email('support@classroomagenda.com', 'Support message')
    @body = { :email => email, :body => body } 
  end
  
  def contact_teacher(to, from_name, body)
    setup_email(to, 'Classroom Agenda recommendation')
    @body = {:from_name => from_name, :body => body }
  end
end

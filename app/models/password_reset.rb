class PasswordReset < ActiveRecord::Base
  include TokenGenerator
  belongs_to :user
  after_create :send_email

  def expired?
    created_at < 2.hours.ago
  end
  
  protected
  
    def send_email
      SubscriptionNotifier.deliver_password_reset(self)
    end
end

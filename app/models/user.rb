require 'digest/sha1'
class User < ActiveRecord::Base
  include ValidationMessage
  extend CreateDemo
  #after_create :send_welcome_email
  has_one :teacher, :dependent => :destroy

  # Virtual attribute for the unencrypted password
  attr_accessor :password
                                                                                       
  validates_presence_of     :login                           
  validates_presence_of     :email                           
  validates_presence_of     :password,                        :if => :password_required?
  validates_presence_of     :password_confirmation,           :if => :password_required?
  validates_length_of       :password, :within => 4..40,      :if => :password_required?
  validates_confirmation_of :password,                        :if => :password_required?
  validates_length_of       :login,    :within => 3..40,      :allow_blank => true
  validates_format_of       :login,    :with => /^[a-zA-Z0-9_]{1,200}$/, :on => :create, :allow_blank => true, :message => "contains invalid characters"
  
  validates_length_of       :email,    :within => 3..100
  validates_format_of       :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create  
  validates_uniqueness_of   :login, :case_sensitive => false
  validates_uniqueness_of   :email, :case_sensitive => false
  validates_exclusion_of    :login, :in => %w( admin www mail cdn ), :message => "has already been taken"
  before_save :encrypt_password


  attr_accessor :domain, :photo_url
  # prevents a user from submitting a crafted form that bypasses activation
  # anything else you want your user to change should be added here.
  attr_accessible :login, :email, :password, :password_confirmation, :identifier, :first_name, :last_name

  # Authenticates a user by their login name and unencrypted password.  Returns the user or nil.
  def self.authenticate(login, password)
    u = find_by_login(login) # need to get the salt
    u && u.authenticated?(password) ? u : nil
  end
  
  # Encrypts some data with the salt.
  def self.encrypt(password, salt)
    Digest::SHA1.hexdigest("--#{salt}--#{password}--")
  end

  # Encrypts the password with the user salt
  def encrypt(password)
    self.class.encrypt(password, salt)
  end

  def authenticated?(password)
    crypted_password == encrypt(password)
  end

  def remember_token?
    remember_token_expires_at && Time.now.utc < remember_token_expires_at 
  end

  # These create and unset the fields required for remembering users between browser closes
  def remember_me
    remember_me_for 2.weeks
  end

  def remember_me_for(time)
    remember_me_until time.from_now.utc
  end

  def remember_me_until(time)
    self.remember_token_expires_at = time
    self.remember_token            = encrypt("#{email}--#{remember_token_expires_at}")
    save(false)
  end

  def forget_me
    self.remember_token_expires_at = nil
    self.remember_token            = nil
    save(false)
  end

  # Returns true if the user has just been activated.
  def recently_activated?
    @activated
  end
  
  def full_name
    "#{first_name} #{last_name}"
  end
  
  def full_name=(name)
    #fill names if we have 2 entries
    names = name.split unless name.nil?
    if !names.nil? && names.length == 2
      self.first_name = names[0]
      self.last_name = names[1]
    end
  end

  def login_text
    identifier.nil? ? "domain" : "login"
  end
  
  HUMANIZED_ATTRIBUTES = { :login => "Username" }
  
  def self.human_attribute_name(attr)
    HUMANIZED_ATTRIBUTES[attr.to_sym] || super
  end
  
  def validation_message
    action = self.new_record? ? "setting up" : "updating"
    "We had some problems #{action} your account"
  end

  protected
    # before filter 
    def encrypt_password
      return if password.blank?
      self.salt = Digest::SHA1.hexdigest("--#{Time.now.to_s}--#{login}--") if new_record?
      self.crypted_password = encrypt(password)
    end
    
    def password_required?
      return false if !identifier.nil?
      crypted_password.blank? || !password.blank?
    end
    
    def send_welcome_email
      SubscriptionNotifier.deliver_welcome(self)
    end
end

class ClassroomGroup < ActiveRecord::Base
  include ValidationMessage
  belongs_to :teacher
  acts_as_list :scope => :teacher
  has_many :classrooms, :dependent => :destroy
  accepts_nested_attributes_for :classrooms, :allow_destroy => true, :reject_if => proc { |attrs| attrs.all? { |k, v| v.blank? } }
  default_scope :order => 'classroom_groups.position'
  validates_presence_of :name
  
  def has_multiple_classrooms?
    classrooms.count > 1
  end
  
  def validation_message
    action = self.new_record? ? "creating" : "updating"
    "We had a problem #{action} this term."
  end

end

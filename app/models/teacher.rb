require 'open-uri'

class Teacher < ActiveRecord::Base
  include TeacherMethods
  include ValidationMessage  
  belongs_to :user
  has_many :messages, :dependent => :destroy
  has_many :classroom_groups, :dependent => :destroy
  has_many :classrooms, :through => :classroom_groups
  has_many :announcements, :dependent => :destroy
  has_many :subscription_payments
  has_one :subscription, :dependent => :destroy
  has_attached_file :photo, :styles => { :small => "150x150>", :extra_small => "50x50>" }
  
  acts_as_polymorphic_paperclip
  
  validate_on_create :valid_plan?
  validate_on_create :valid_payment_info?
  validate_on_create :valid_subscription?
  
  attr_accessible :name, :domain, :plan, :plan_start, :creditcard, :address, :salutation, :bio, :photo, :display_announcements, :display_assignments, :display_calendar, :display_exams, :display_field_trips
  attr_accessor :plan, :plan_start, :creditcard, :address, :remote_url

  #validates_uniqueness_of :subdomain, :case_sensitive => false, 
  #   :message => "is taken, please choose a unique subdomain"
     
  #def full_name
  #  "#{salutation} #{first_name} #{last_name}"
  #end 
  
  def self.find_by_subdomain(subdomain)
    u = User.find_by_login(subdomain)
    u.teacher unless u.nil?
  end  
    
  def needs_payment_info?
    if new_record?
      AppConfig['require_payment_info_for_trials'] && @plan && @plan.amount.to_f + @plan.setup_amount.to_f > 0
    else
      self.subscription.needs_payment_info?
    end
  end  

  def active?
    self.subscription.next_renewal_at >= Time.now
  end
  
  def has_premium_expired_trial?
    self.subscription.subscription_plan.name == "Premium" && self.subscription.state == "trial" && self.subscription.next_renewal_at < DateTime.now
  end

  def download_remote_image(url)
    io = open(URI.parse(url))
    def io.original_filename; base_uri.path.split('/').last; end
    self.photo = io.original_filename.blank? ? nil : io
  rescue # catch url errors with validations instead of exceptions (Errno::ENOENT, OpenURI::HTTPError, etc...)
  end

protected

    def valid_payment_info?
      if needs_payment_info?
        unless @creditcard && @creditcard.valid?
          errors.add_to_base("Invalid payment information")
        end
        
        unless @address && @address.valid?
          errors.add_to_base("Invalid address")
        end
      end
    end
    
    def valid_plan?
      errors.add_to_base("Invalid plan selected.") unless @plan
    end
    
    def valid_subscription?
      return if errors.any? # Don't bother with a subscription if there are errors already
      self.build_subscription(:plan => @plan, :next_renewal_at => @plan_start, :creditcard => @creditcard, :address => @address)
      if !subscription.valid?
        errors.add_to_base("Error with payment: #{subscription.errors.full_messages.to_sentence}")
        return false
      end
    end

end

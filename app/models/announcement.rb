class Announcement < ActiveRecord::Base
  include ValidationMessage  
  belongs_to :teacher
  named_scope :todays, lambda { { :conditions => ['start_date <= ? AND end_date >= ?', Date.current, Date.current] } }
  validates_presence_of :title
  acts_as_polymorphic_paperclip
  include Assetable #needs to be last in - overrides after_save - poly paperclip does as well.
  
  def attachment_limit_reached?
    return false if teacher.has_premium_plan?
    teacher.assets.count >= AttachmentLimit
  end
  
  def check_timestamps
    false
  end
end

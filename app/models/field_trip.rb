class FieldTrip < ActiveRecord::Base
  include CheckTimestamps  
  include ValidationMessage
  has_and_belongs_to_many :classrooms
  validates_presence_of :classrooms, :message => "^At least one classroom must be selected"
  validates_presence_of :title
  default_scope :order => 'field_trips.trip_date DESC'
  acts_as_polymorphic_paperclip
  include Assetable #needs to be last in - overrides after_save - poly paperclip does as well.
  
  def target_date
    trip_date
  end
end

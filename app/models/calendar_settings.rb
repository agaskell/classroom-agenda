class CalendarSettings
  attr_accessor :show_announcements, :show_assignments, :show_exams, :show_field_trips, :c

  def show_announcements?
    return show_announcements == 1
  end

  def show_assignments?
    return show_assignments == 1
  end

  def show_exams?
    return show_exams == 1
  end
  
  def show_field_trips?
    return show_field_trips == 1
  end
  
  def initialize(attributes = nil)
    if attributes.nil? || attributes["c"].nil?
      self.c = Hash.new(1)
    else
      self.c = attributes["c"].inject({}) { |hash, obj| hash[obj[0].to_i] = obj[1].to_i; hash }  #.map { |k, v| k = k.to_i, v = v.to_i }.flatten]
    end
    
    params.each do |key, value| 
      if attributes.nil? || attributes[key].nil?
        self.send("#{value}=", 1)
      else
        self.send("#{value}=", attributes[key].to_i) unless attributes[key].nil? 
      end
    end
  end
  
  def disabled_classrooms
    return c.select { |key, value| value == 0 }.map { |c| c[0] }
  end
  
private
  def params
    @@params ||= { "an" => :show_announcements, "as" => :show_assignments, "ex" => :show_exams, "ft" => :show_field_trips }
  end
end

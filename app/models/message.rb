class Message < ActiveRecord::Base
  include ValidationMessage
  belongs_to :teacher
  default_scope :order => 'messages.created_at DESC'
  validates_presence_of :body, :message => "^Message can't be blank"
  validates_presence_of :email, :message => "^Please enter your name or email"
  after_create :send_email

  def validation_message
    "There were some problems sending your message"
  end


  protected
    def send_email
      SubscriptionNotifier.deliver_message_notification(self)
    end
end

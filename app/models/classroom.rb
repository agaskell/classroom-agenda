class Classroom < ActiveRecord::Base
  include ValidationMessage
  belongs_to :classroom_group
  acts_as_list :scope => :classroom_group

  has_and_belongs_to_many :assignments
  has_and_belongs_to_many :field_trips
  has_and_belongs_to_many :exams  

  default_scope :order => 'classrooms.position'
  
  default_value_for :display, true
  validates_presence_of :name
  
  def to_s
    name
  end
end

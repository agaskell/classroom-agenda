module TeachersHelper
  
  def calendar_events_proc(teacher)
    lambda do |day|
      if teacher.event_on?(day)
        [link_to(day.day, url_for(:controller => "teachers", 
          :action => :show_date, 
          :year => day.year, :month=>sprintf("%02d", day.month), :day=>sprintf("%02d", day.day),
          "an" => teacher.calendar_settings.show_announcements, 
          "as" => teacher.calendar_settings.show_assignments,
          "ex" => teacher.calendar_settings.show_exams,
          "ft" => teacher.calendar_settings.show_field_trips), { :class => "dayWithEvents" })]
      else
        day.day
      end
    end
  end
  
  def calendar_day_url(teacher, day)
    url_for(:controller => "teachers", 
          :action => :show_date, 
          :year => day.year, :month=>sprintf("%02d", day.month), :day=>sprintf("%02d", day.day),
          "an" => teacher.calendar_settings.show_announcements, 
          "as" => teacher.calendar_settings.show_assignments,
          "ex" => teacher.calendar_settings.show_exams,
          "ft" => teacher.calendar_settings.show_field_trips,
          "c" =>  teacher.calendar_settings.c)  
  end
  
  def calendar_cell(teacher, day, month)
    weekend = day.wday == 6 || day.wday == 0 ? "ui-datepicker-week-end " : ""
    if day.month == month 
      if teacher.event_on?(day)
        "<td class=''><a href='#{calendar_day_url(teacher, day)}' class='ui-state-default'>#{day.day}</a></td>"    
      else
        "<td class=''><span class='ui-state-disabled'>#{day.day}</span></td>"    
      end
    else
      "<td class='#{weekend}ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled'> </td>"
    end
  end
  
  def happening_text(date = Date.current)
    d = case date
      when Date.yesterday then "yesterday, "
      when Date.today then "today, "
      when Date.tomorrow then "tomorrow, "
      else "on"
      end
    date >= Date.current ? "What's happening #{d} " : "What happened #{d} "
  end
  
  def calendar_help_link(text, anchor)
    link_to(text, calendar_help_path(:anchor => anchor) )
  end
  
  def teacher_url(t)
    teacher_root_url(:subdomain => t.user.login)
  end
  
end

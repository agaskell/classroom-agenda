module UsersHelper
  def classrooms_settings_url
    logged_in_teacher.active_classroom_group.nil? ? needs_active_term_admin_classroom_groups_path : admin_classroom_group_classrooms_path(logged_in_teacher.active_classroom_group)    
  end
  
  def assignments_settings_url
    if logged_in_teacher.active_classroom_group.nil?
      needs_active_term_admin_classroom_groups_path
    elsif logged_in_teacher.active_classrooms.length == 0
      needs_at_least_one_classroom_path
    else 
      admin_assignments_path
    end
  end
end

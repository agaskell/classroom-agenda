module AssignmentsHelper
  def due_sentence(assignment)
    sentence = "Assignment <strong>#{h(assignment.title)}</strong> is due"
    if assignment.classrooms.length > 0 && assignment.classrooms[0].classroom_group.has_multiple_classrooms?
      "#{sentence} for #{h(assignment.classrooms.to_sentence)}."
    else
      "#{sentence}."
    end
  end
  
  def assignment_updated_sentence(assignment)
    action = (assignment.created_at == assignment.updated_at ? "created" : "updated")
    sentence = sentence_icon("sx_icons_#{action}") + "<span class='sentence_indent'>Assignment <strong>#{h(assignment.title)}</strong> #{action}. Due date is set for #{assignment.due_date}.</span>"
  end  
end

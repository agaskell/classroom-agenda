module UrlHelper

  def settings_url
    #settings_user_path(current_user, :subdomain => current_user.login)
   teacher_settings_path(:subdomain => current_user.login)
  end

  def calendar_url(date = Date.current)
    
    url_for(:controller => "/teachers", 
            :action => :show_date, 
            :year => date.year, :month=>sprintf("%02d", date.month), :day=>sprintf("%02d", date.day))
  end
  
  def menu_link_to(*args, &block)
    controller = args.shift
    action = args.shift

    if controller == controller_name && (action.nil? || action == action_name)
      if args.third.nil?
        args.push({:class => 'selected'})
      else
        args.third.merge!({:class => 'selected'})
      end
    end

    link_to *args, &block
  end

  def permission_link_to(*args, &block)
    if current_user.teacher.has_premium_plan?
      link_to args, block
    else
      "<span class='disabledLink'>#{args.first}</span>"
    end
  end

end

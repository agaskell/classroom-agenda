module ExamsHelper
  def exam_sentence(exam)
    sentence = "Exam <strong>#{h(exam.title)}</strong>"
    if exam.classrooms.length > 0 && exam.classrooms[0].classroom_group.has_multiple_classrooms?
      "#{sentence} for #{h(exam.classrooms.to_sentence)}."
    else
      "#{sentence}."
    end
  end  
    
  def exam_updated_sentence(exam)
    action = exam.created_at == exam.updated_at ? "created" : "updated"
    sentence = sentence_icon("sx_icons_#{action}") +  "<span class='sentence_indent'>Exam <strong>#{h(exam.title)}</strong> #{action}. Exam date is set for #{exam.exam_date}.</span>"
  end
end

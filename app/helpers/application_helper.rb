# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  include UrlHelper
  include TeachersHelper

  def sentence_icon(css_class)
    "<img src=\"/images/spacer.gif\" class=\"spritemap_icons #{css_class}\" alt=\"\"/>"
  end

FLASH_NOTICE_KEYS = [:error, :notice, :warning]

  def flash_messages
    return unless messages = flash.keys.select{|k| FLASH_NOTICE_KEYS.include?(k)}
    formatted_messages = messages.map do |type|      
      content_tag :div, :class => type.to_s do
        message_for_item(flash[type], flash["#{type}_item".to_sym])
      end
    end
    formatted_messages.join
  end

  def message_for_item(message, item = nil)
    if item.is_a?(Array)
      message % link_to(*item)
    else
      message % item
    end
  end
  
  def logged_in_teacher
    return false if current_user.nil?
    @logged_in_teacher ||= current_user.teacher
  end  
  
  def current_teacher
    @current_teacher ||= Teacher.find_by_subdomain(current_subdomain)
    if(@current_teacher.nil?)
      flash[:error] = "Invalid account"
      redirect_to root_url(:subdomain => false)
    end
    @current_teacher
  end  
  
  def css_selected_if_current(controller)
    return "selected" if controller == controller_name #current_page?(:controller => controller)
  end
end

module FieldTripsHelper
  def field_trip_sentence(field_trip)
    sentence = "Field Trip <strong>#{h(field_trip.title)}</strong>"
    if field_trip.classrooms.length > 0 && field_trip.classrooms[0].classroom_group.has_multiple_classrooms?
      "#{sentence} for #{h(field_trip.classrooms.to_sentence)}."
    else
      "#{sentence}."
    end
  end
  
  def field_trip_updated_sentence(field_trip)
    action = field_trip.created_at == field_trip.updated_at ? "created" : "updated"
    sentence = sentence_icon("sx_icons_#{action}") + "<span class='sentence_indent'>Field Trip <strong>#{h(field_trip.title)}</strong> #{action}. Trip date is set for #{field_trip.trip_date}.</span>"
  end    
end

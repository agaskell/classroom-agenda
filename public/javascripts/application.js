(function() {
  var originalRemoveMethod = jQuery.fn.remove;
  jQuery.fn.remove = function() {
    if(this.hasClass("infomenu") || this.hasClass("pop")) {
      $(".selected").removeClass("selected");
    }
    originalRemoveMethod.apply(this, arguments);
  }
})();

function fetchDate(url) {
  $("#main").slideFadeToggle();
  var options_visible = $(".options").is(":visible");
  $.get(url, function(data) {
    $("#main").html("").append($(data).filter(".announcements")).append($(data).filter(".agenda")).slideFadeToggle(); ;
    document.title = $(data).filter("title").text();
    updateSidebar($(data));
    if(!options_visible) { $(".hideme").hide(); }
    setHoverInfo();
  });  
}

function getSelectName(select) {
  var startIndex = select.name.indexOf("(");
  var endIndex = select.name.indexOf(")", startIndex);
  return select.name.substring(0, startIndex) + select.name.substring(endIndex + 1);
}

function setSortables() {
  $("tbody.sorty").sortable({ 
    handle: ".handle",
    update: function() { 
      $.post(window.location + "/sort", "_method=put&" + $("tbody.sorty").sortable('serialize'));
    }
  });
  $("tbody.sorty").disableSelection();
  $("tbody.sorty tr[class='hasButtons']").prepend('<td><img alt="Click here to drag" title="Click here to drag" src="/images/arrow.png" class="handle" /></td>');
  $(".hasButtons").removeClass("hasButtons");
  $(".removeMe").remove();
}

function setCalendars() {
  var dateSelects = $("select.date_select");
  var newName, oldName;
  
  for(var i = 0; i < dateSelects.length; i++) {
    newName = getSelectName(dateSelects[i]);

    if(newName != oldName && oldName !== undefined) {
      var d = $(dateSelects[i -1]).parent().children(".date_value").text();
      $(dateSelects[i -1]).parent().append('<input type="textbox" name="' + oldName + '" value="' + d + '" class="datepicker" />');
    }

    oldName = newName; 
  }
  
  var d = $(dateSelects[i -1]).parent().children(".date_value").text();
  $(dateSelects[dateSelects.length - 1]).parent().append('<input type="textbox" name="' + oldName + '" value="' + d + '" class="datepicker" />');
  
  $("select.date_select").remove();
  $(".datepicker").datepicker();
}

function setHoverInfo() {
  $(".info").hide()

  $(".statuses li").hover(function() { 
    if($(".infomenu").length == 0) {
      $(this).find(".info").show();
    }
  },
  function() {
    if($(".infomenu").length == 0) {
      $(".info").hide();
    }
  });  
}

function closePop(fn) {
  var arglength = arguments.length;
  if($(".pop").length == 0) { return false; }
  $(".pop").slideFadeToggle(function() { 
    if(arglength) { fn.call(); }
    $(this).remove();
  });    
  return true;
}

function isPost(requestType) {
  return requestType.toLowerCase() == 'post';
}

function hasErrors(response) {
  if(response === null) { return false; }

  var errorElement = "id=\"errorExplanation\"";

  if(typeof(response) == "string") {
    return response.indexOf(errorElement) > -1;
  } 
}

function clearPopups() {
  $('.infomenu').remove(); 
}

function updateUnreadMessageCount(data) {
  var unread = $(data).filter("#unread_message_count").val();
  if(unread !== undefined) {
    $("#messages").text("Messages (" + unread + ")");
  }  
}

function newForm() {
  $("#new_classroom_group, #new_classroom").ajaxForm({
    success: function(responseText) {
      var response = $(responseText);
      var newrow = response.filter("tr");
      if(newrow.length > 0) {      
        if(newrow.find("td.active").length) {
          $("table td.active").text($("table td.active").text().replace("(currently active)", ""));
          $("table td").removeClass("active");
        }

        if($("table tbody tr:last").length) {
          newrow.insertAfter("table tbody tr:last").effect("highlight", {}, 1500);
        } else {
          newrow.appendTo("table tbody").effect("highlight", {}, 1500);        
        }
        
        closePop();
      } else {
        $(".newpop").html(responseText);
        newForm();
      }
    }
  });
}

function editForm() {
  $(".edit_classroom_group, .edit_classroom").ajaxForm({
    success: function(responseText) {
      var response = $(responseText);
      var newrow = response.filter("tr");
      if(newrow.length > 0) {      
        if(newrow.find("td.active").length) {
          $("table td.active").text($("table td.active").text().replace("(currently active)", ""));
          $("table td").removeClass("active");
        }
        var id = newrow.attr("id");
        $("tr#" + id).replaceWith(newrow);
        $("#" + id).effect("highlight", {}, 1500);
        closePop();
      } else {
        $(".editpop").html(responseText);
        editForm();        
      }
    }
  }); 
}

function getId(cb) {
  if(cb.id == "as") {
    return "assignments";    
  } else if(cb.id == "ex") {
    return "exams";    
  } else if(cb.id == "ft") {
    return "field_trips";    
  }
  return null; 
}

function updateSidebar(data) {
  $("#calendar").html(data.filter("#calendar").html());
  $("#export_url").val(data.find("#export_url").val());
  $("#calendar_settings").replaceWith(data.filter("#calendar_settings"));
  $("#export_url").focus(function() { this.select(); }).mouseup(function(e){ e.preventDefault(); });
  $("#calendar_settings_submit").hide();
}

function contactTeacherForm() {
  $("#new_message").ajaxForm({
    success: function(responseText) {
      if(hasErrors(responseText)) {      
        $(".messagepop").html(responseText);
        contactTeacherForm();
      } else {
        closePop();
      }
    }
  });
}  

function filterTable() {
  $("#classrooms_checkboxes :checkbox").each(function() {
    var val = parseInt($(this).val());
    var checked = $(this).is(':checked');
    
    $('#table tr').each(function() {
      var row = $(this);
      var txt = row.find("td:first").text();
      if(txt.length) {
        var json = eval('(' + txt + ')');
        
        for(var i = 0; i < json.length; i++) {
          var hasOtherChecks = false;
          for(var j = 0; j < json.length; j++) {
            if(i != j) {
              hasOtherChecks = $('#' + json[j].classroom.id).is(':checked');
            }
            if(hasOtherChecks) { break; }
          }
          
          if(json[i].classroom.id === val) {
            if(checked && row.is(":hidden")) {
              row.show();
            }
            else if(!checked && row.is(":visible") && !hasOtherChecks) {
              row.hide();
            }
          }
        }
        
      }
    });
    
    $('#table tr:visible').each(function(i) {
      var c = i % 2 == 0 ? "even" : "odd";
      $(this).attr("class", c);
    });
  });
}


$(function() {
  $(".hideme").hide();
  
  $(".pop").unload(function() { 
    alert("later!");
  });

  $(".pop").live('click', function(e) { e.stopPropagation(); });
  
  setHoverInfo();
  setSortables();
  setCalendars();
  filterTable();

  $('a.get').live('click', function() {
    var link = $(this);
    $.get(link.attr('href'), function(data) {
      if (link.attr('ajaxtarget')) {
        $(link.attr('ajaxtarget')).html(data).slideDown("slow");        
      }
      else {
        $("#main").html(data);
      }
    });
    return false;
  }).attr("rel", "nofollow");

  $(document).ajaxError(function(event, xhr, settings, error) { 
    alert(error); 
  });
    
  $(document).ajaxSuccess(function(event, xhr, settings) { 
	  if(isPost(settings.type) && hasErrors(xhr.responseText) === false)  {
      setSortables();
	  } else {
  		setCalendars();
	  }
  });
  
  $(document).ajaxSend(function(event, xhr, settings) {
	  if (isPost(settings.type)) {
      settings.data = (settings.data ? settings.data + "&" : "") + "authenticity_token=" + encodeURIComponent( AUTH_TOKEN );
    }
    settings.url += ".js";
    xhr.setRequestHeader("Accept", "text/javascript, application/javascript");     
	});

    
  $(document).click(function(e) {
    if(e.isPropagationStopped() === false) { 
      clearPopups();  
      $('.info').hide();
    }
  });
  
  $("a.show").live('click', function() {
    clearPopups();
    $(this).addClass("selected").parent().append("<div class='infomenu list loading'></div>");
    $.get(this.href, function(data) {
      $(".infomenu").removeClass('loading').html(data);
      updateUnreadMessageCount(data);
    });
    return false;
  });
  
  $("a.feed_item").live('click', function(event) {
    if(event.button != 0) { return true; }
    var list_item = $(this).parents("li:first");
    
    if(list_item.find(".drop").length) {
      list_item.find(".drop").slideFadeToggle();
      return false;
    }
    else { 
      list_item.append("<div class='drop'><a href='#' class='close_drop'>Close</a></div>");
    }
    
    $.get(this.href, function(data) {
      list_item.find(".drop").prepend(data).slideFadeToggle();
    });
    return false;
  });
  
  $("a.close_drop").live('click', function() {
    $(this).parents(".drop").slideFadeToggle();
  });
  
  $("a.new").live('click', function(event) {
    if(event.button != 0) { return true; }
    clearPopups();  
    var link = $(this);
    link.addClass("selected").parent().append("<div class='pop newpop'></div>");
    $.get(this.href, function(data) {
      $(".newpop").html(data).slideFadeToggle(function() { }); //setAjaxForm(); }); 
      $(".newpop input[type=text]:first").focus(); // check with IE - may need to call setTimeout setTimeout(function() { $(".newpop input[type=text]:first").focus(); }, 500);
      newForm();
    });
    return false;
  });
  
  $('a.edit').live('click', function(event) {
    if(event.button != 0) { return true; }
    clearPopups();  
    $(this).addClass("selected").parent().append("<div class='pop editpop'></div>");
    $.get(this.href, function(data) {
      $(".editpop").html(data).slideFadeToggle(function() { }); //setAjaxForm(); }); 
      $(".editpop input[type=text]:first").focus().select();
      editForm();
    });
    return false;
  });
    
  $('a.delete').live('click', function(event) {
    if(event.button != 0) { return true; }
    clearPopups();  

    var link = $(this);
    link.addClass("selected").parent().append("<div class='pop delpop'><p>Are you sure?</p><p><input type='button' value='Yes' /> or <a href='#' class='close'>Cancel</a></div>");
    $(".delpop").slideFadeToggle();
    
    $(".delpop input").click(function() {
      $(".pop").slideFadeToggle(function() { 
      $.post(link.attr('href').substring(0, link.attr('href').indexOf('/confirm_destroy')), { _method: "delete" },
        function(response) { 
          link.parents("tr").fadeOut(function() { 
            $(this).remove();
          });
          updateUnreadMessageCount(response);
        });
        $(this).remove();
      });    
    });
    
    return false;
  });

  $("#all").click(function() {
    $("#classrooms_checkboxes").find(':checkbox').not(':checked').click().change(); //.attr('checked', 'checked');
    return false;
  });
  
  $("#none").click(function() {
    $("#classrooms_checkboxes").find(':checkbox:checked').click().change(); // removeAttr('checked');
    return false;
  });
  
  $("#classrooms_checkboxes :checkbox").change(filterTable);
  
  $("#right-main").html($("#right-how").html())
  
  $(".teachers-link, .students-link, .parents-link, .how-link").live("click", function() {
    var c = $(this).attr("class");
    var n = c.substring(0, c.indexOf('-'));

    $("#right-main").slideFadeToggle(function() {
      $(this).html($("#right-" + n).html());
      $(this).slideFadeToggle();
    });

    
    
    //$(".right").children().slideFadeToggle(function() {
    //  
    //});
    return false;
  });
  
  /*
  $("#classrooms_checkboxes :checkbox").change(function() {
    //alert('changed');
    var val = $(this).val();
    var checked = $(this).is(':checked');
    $('#table tr').each(function() {
      if($(this).find("td:first").text() == val) {
        if(checked) {
          $(this).fadeIn();
        }
        else {
          $(this).fadeOut();
        }
      }
    });
  });*/
  

  $("#calendar_settings #an").live('click', function() {
    var checkbox = $(this);
    var announcements = $(".announcements");
    
    //var values = $.queryString($("#calendar_settings").serialize());
    //$.setFragment({ "d" : $("#calendar_settings").attr("action"), "an" : values.an, "as" : values.as, "ex" : values.ex, "ft" : values.ft });
    
    $("#calendar_settings").ajaxSubmit(function(data) {
      if(checkbox.is(":checked")) {
        if(announcements.length) {
          announcements.children("#announcements").replaceWith($(data).find("#announcements"));
          announcements.slideFadeToggle(); 
          setHoverInfo();
        } else {
          $("#main").prepend($(data).filter(".announcements").css("display", "none"));
          $(".announcements").slideFadeToggle();
        }
      } else {
        announcements.slideFadeToggle();
      }
      updateSidebar($(data));
    });
  });
  
  $("#calendar_settings input[type=checkbox].e").live('click', function() {
    var checkbox = $(this);
    var agenda_status = $("#main ol.status_heading");
    var id = getId(this);
    var ol = $("#" + id);

    $("#calendar_settings").ajaxSubmit(function(data) {
      if(checkbox.is(":checked")) {
        if(ol.length) {
          ol.replaceWith($(data).find("#" + id).css('display', 'none'));
          $(".status_heading #" + id).slideFadeToggle();
        } else if(agenda_status.length) {
          agenda_status.prepend($(data).find("#" + id).css('display', 'none'));
          $(".status_heading #" + id).slideFadeToggle();
        } else {
          $(".agenda").append($(data).find(".status_heading").css('display', 'none'));
          $(".status_heading").slideFadeToggle();
        }
        setHoverInfo();
      } else {
        ol.slideFadeToggle();
      }
      updateSidebar($(data));
    });
  });
  
  $("#calendar_settings input[type=checkbox].cl").live('click', function() { 
    var checkbox = $(this);
    var agenda_status = $("#main ol.status_heading");    
    
    $("#calendar_settings").ajaxSubmit(function(data) {
      var d = $(data);

      if(checkbox.is(":checked")) { 
        d.find("ol.statuses").each(function() {
          var id = this.id; 
          var ol = $("#" + id);
          var new_ol = d.find("#" + id);
          
          if(ol.html() == new_ol.html()) { return; }

          if(ol.length) {
            ol.replaceWith(new_ol.css('display', 'none'));
            $("#" + id).slideFadeToggle();
          } else if(agenda_status.length) {
            agenda_status.prepend(new_ol.css('display', 'none'));
            $("#" + id).slideFadeToggle();
          } else {
            $(".agenda").append(d.find(".status_heading").css('display', 'none'));
            $(".status_heading").slideFadeToggle();
          }
        });
      } else {
        $("ol.statuses a").each(function() {
          if(d.find("a[href=" + $(this).attr('href') + "]").length == 0) {
            $(this).parent().slideFadeToggle(function() { $(this).remove(); });
          }
        });
      }   
      updateSidebar(d); 
    });
  });
  
  $("#calendar a").live('click', function() {
    //var values = $.queryString(this.href);
    //$.setFragment( { "d" : this.pathname, "an" : values.an, "as" : values.as, "ex" : values.ex, "ft" : values.ft });
    fetchDate(this.href);
    return false;
  });
 
 /* 
  $.fragmentChange(true);
  $(document).bind("fragmentChange.d", function() {
    var f = $.fragment();
    fetchDate(f.d, { "an" : f.an, "as" : f.as, "ex" : f.ex, "ft" : f.ft });
  });
  */
  
  $("#contact_teacher").live('click', function(event) {
    if(event.button != 0) { return true; }
    if($(".messagepop").length) { return false; }
    clearPopups();
    $(this).addClass("selected").parent().append("<div class='messagepop pop'></div>");
    $(".pop").slideFadeToggle()

    $.get(this.href, function(data) {
      $(".messagepop").html(data);
      $("#message_email").focus();
      contactTeacherForm();
    });
    return false;
  });
  
  $("#user_login").keypress(function() {
    var login = this;
    if (this.value != this.lastValue) {
      $("#loading_small").addClass("loading-small");
      if (this.timer) { clearTimeout(this.timer); }
      this.timer = setTimeout(function() { 
        $.get("/users/login_validate", $(login).serialize(), function(data) {
          $("#loading_small").removeClass("loading-small");
          $("#login_valid").fadeIn().html(data);
        });
      }, 500);
    }
  });
  
  $(".close").live('click', function() {
    return !closePop();
  });
  
  $("a.results").live('click', function() {
    var link = $(this);
    var text = link.text();
    link.text("Loading...");
    $.get(this.href, function(data) {
      var title = $(data).filter("title").text();
      document.title = title;
      $("#main").slideFadeToggle(function() {
        $(this).html(data);
        $(this).slideFadeToggle();
        filterTable();
      });
    });
    return false;
  });
  
  $("#export_url").focus(function() { this.select(); }).mouseup(function(e){ e.preventDefault(); }); 
  
  $("#advanced_options").live('click', function(event) {
    $(".options").slideFadeToggle();
    return false;
  });
  
  

});

$.fn.slideFadeToggle = function(easing, callback) {
  return this.animate({opacity: 'toggle', height: 'toggle'}, "fast", easing, callback);
};
